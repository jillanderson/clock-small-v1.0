#pragma once

/** \file ********************************************************************
 *
 * timer1 niskopoziomowe sterowanie w trybie CTC
 *
 *****************************************************************************/

#include <stdint.h>

/**
 * uruchamia timer w trybie CTC mode 4 i włącza przerwanie TIMER1_COMPA_vect
 * oraz TIMER1_COMPB_vect
 *
 * TCCR1B - Timer/Counter Control Register:
 * 	- CS12 CS11 CS10
 * 	- 0 0 0 No clock source (Timer/Counter stopped).
 * 	- 0 0 1 clk T2S /1 (No prescaling)
 * 	- 0 1 0 clk T2S /8 (From prescaler)
 * 	- 0 1 1 clk T2S /64 (From prescaler)
 * 	- 1 0 0 clk T2S /256 (From prescaler)
 * 	- 1 0 1 clk T2S /1025 (From prescaler)
 *
 * \param prescale	maska bitowa prescalera 0-5
 * \param compare	wartość porównania dla licznika
 *
 */
void hal_timer1_start(uint8_t prescale, uint16_t top, uint16_t compare);

/**
 * wyłacza timer
 *
 * resetuje ustawienia oraz flagę przerwania
 *
 */
void hal_timer1_stop(void);

/**
 * rejestruje funkcję zwrotną dla uchwytu przerwania timera
 *
 * \param handler	funkcja zwrotna klienta uchwytu przerwania
 *
 */
void hal_timer1_add_compare_handler(void (*handler_comp_a)(void),
		void (*handler_comp_b)(void));

