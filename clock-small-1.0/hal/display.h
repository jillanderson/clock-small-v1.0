#pragma once

/** \file ********************************************************************
 *
 * sterowanie anodami i segmentami wyświetlaczy i dwukropków
 *
 * - przed użyciem należy wywołać funkcję konfiguracji gpio
 *
 *****************************************************************************/

#include <stdbool.h>

typedef enum anode_id {
	ANODE_0,
	ANODE_1,
	ANODE_2,
	ANODE_3,
	ANODE_4,
	ANODE_5,
	ANODE_COLON_0,
	ANODE_COLON_1
} anode_id_t;

typedef enum segment_id {
	SEGMENT_A,
	SEGMENT_B,
	SEGMENT_C,
	SEGMENT_D,
	SEGMENT_E,
	SEGMENT_F,
	SEGMENT_G,
	SEGMENT_COLON
} segment_id_t;

/**
 * włącza zasilanie anody
 *
 * \param id	identyfikator anody
 *
 */
void hal_display_anode_turn_on(anode_id_t id);

/**
 * wyłącza zasilanie anody
 *
 * \param id	identyfikator anody
 *
 */
void hal_display_anode_turn_off(anode_id_t id);

/**
 * włącza zasilanie segmentu
 *
 * \param id	identyfikator segmentu
 *
 */
void hal_display_segment_turn_on(segment_id_t id);

/**
 * wyłącza zasilanie segmentu
 *
 * \param id	identyfikator segmentu
 *
 */
void hal_display_segment_turn_off(segment_id_t id);

