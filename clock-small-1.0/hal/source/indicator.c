#include "../indicator.h"

//#include <stdint.h>

#include "../../common/avr/gpio.h"
#include "../../common/gcc/utils.h"
#include "../gpio.h"

static gpio_handle_t *const indicator[] = {&((gpio_handle_t){INDICATOR_0}), &((gpio_handle_t){INDICATOR_1})};

void hal_indicator_turn_on(indicator_id_t id)
{
	if (array_size(indicator) > id) {
		gpio_pin_out_set_hi(indicator[id]);
	}
}

void hal_indicator_turn_off(indicator_id_t id)
{
	if (array_size(indicator) > id) {
		gpio_pin_out_set_lo(indicator[id]);
	}
}

void hal_indicator_toggle(indicator_id_t id)
{
	if (array_size(indicator) > id) {
		gpio_pin_out_toggle(indicator[id]);
	}
}

