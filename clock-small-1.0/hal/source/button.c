#include "../button.h"

#include <stdbool.h>

#include "../../common/gcc/utils.h"
#include "../../common/avr/gpio.h"
#include "../gpio.h"

static gpio_handle_t *const button[] = {&((gpio_handle_t){BUTTON_0}),
					&((gpio_handle_t){BUTTON_1}),
					&((gpio_handle_t){BUTTON_2}),
					&((gpio_handle_t){BUTTON_3}),
					&((gpio_handle_t){BUTTON_4})};

bool hal_button_is_pressed(button_id_t id)
{
	bool result = false;

	if (array_size(button) > id) {
		result = gpio_pin_in_is_lo(button[id]);
	}

	return (result);
}

bool hal_button_is_released(button_id_t id)
{
	bool result = false;

	if (array_size(button) > id) {
		result = gpio_pin_in_is_hi(button[id]);
	}

	return (result);
}

