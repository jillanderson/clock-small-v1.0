#include "../timer2.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stddef.h>

#include "../../common/general/bits.h"

static void (*callback_a)(void);
static void (*callback_b)(void);

void hal_timer1_start(uint8_t prescale, uint16_t top, uint16_t compare)
{
	BIT_MASK_OUT(TCCR1B, (BIT_VALUE(WGM12) | BIT_FIELD(prescale,3U,0U)));
	BIT_MASK_OUT(TCNT1, 0U);
	BIT_MASK_OUT(OCR1A, top);
	BIT_MASK_OUT(OCR1B, compare);
	BIT_SET(TIMSK, OCIE1A);
	BIT_SET(TIMSK, OCIE1B);
}

void hal_timer1_stop(void)
{
	BIT_MASK_OUT(TCCR1A, 0U);
	BIT_MASK_OUT(TCCR1B, 0U);
	BIT_MASK_OUT(OCR1A, 0U);
	BIT_MASK_OUT(OCR1B, 0U);
	BIT_CLEAR(TIMSK, OCIE1A);
	BIT_CLEAR(TIMSK, OCIE1B);
	/* flagę przerwania kasujemy przez wpisanie jedynki */
	BIT_SET(TIFR, OCF1A);
	BIT_SET(TIFR, OCF1B);
}

void hal_timer1_add_compare_handler(void (*handler_comp_a)(void),
		void (*handler_comp_b)(void))
{
	callback_a = handler_comp_a;
	callback_b = handler_comp_b;
}

ISR(TIMER1_COMPA_vect)
{
	if (NULL != callback_a) {
		callback_a();
	} else {
		;
	}
}

ISR(TIMER1_COMPB_vect)
{
	if (NULL != callback_b) {
		callback_b();
	} else {
		;
	}
}

