#include "../display.h"

#include "../../common/gcc/utils.h"
#include "../../common/avr/gpio.h"
#include "../gpio.h"

static gpio_handle_t *const anode[] = {&((gpio_handle_t){DISP_ANODE_0}),
					&((gpio_handle_t){DISP_ANODE_1}),
					&((gpio_handle_t){DISP_ANODE_2}),
					&((gpio_handle_t){DISP_ANODE_3}),
					&((gpio_handle_t){DISP_ANODE_4}),
					&((gpio_handle_t){DISP_ANODE_5}),
					&((gpio_handle_t){DISP_COLON_ANODE_0}),
					&((gpio_handle_t){DISP_COLON_ANODE_1})};


static gpio_handle_t *const segment[] = {&((gpio_handle_t){DISP_SEG_A}),
					&((gpio_handle_t){DISP_SEG_B}),
					&((gpio_handle_t){DISP_SEG_C}),
					&((gpio_handle_t){DISP_SEG_D}),
					&((gpio_handle_t){DISP_SEG_E}),
					&((gpio_handle_t){DISP_SEG_F}),
					&((gpio_handle_t){DISP_SEG_G}),
					&((gpio_handle_t){DISP_COLON})};

void hal_display_anode_turn_on(anode_id_t id)
{
	if (array_size(anode) > id) {
		gpio_pin_out_set_hi(anode[id]);
	}
}

void hal_display_anode_turn_off(anode_id_t id)
{
	if (array_size(anode) > id) {
		gpio_pin_out_set_lo(anode[id]);
	}
}

void hal_display_segment_turn_on(segment_id_t id)
{
	if (array_size(segment) > id) {
		gpio_pin_out_set_hi(segment[id]);
	}
}

void hal_display_segment_turn_off(segment_id_t id)
{
	if (array_size(segment) > id) {
		gpio_pin_out_set_lo(segment[id]);
	}
}

