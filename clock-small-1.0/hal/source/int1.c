#include "../int1.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stddef.h>

static void (*callback)(void);

void hal_external_int1_start(void)
{
	/* przerwanie INT1 wyzwalane zboczem opadającym */
	MCUCR |= (1U << ISC11);
	MCUCR &= ~(1U << ISC10);
	/* włączenie przerwań od INT1, skasowanie flagi przerwania */
	GIFR |= (1U << INTF1);
	GICR |= (1U << INT1);
}

void hal_external_int1_stop(void)
{
	/* włączenie przerwań od INT1, skasowanie flagi przerwania */
	GIFR |= (1U << INTF1);
	GICR &= ~(1U << INT1);
}

void hal_external_int1_add_handler(void (*handler)(void))
{
	callback = handler;
}

ISR(INT1_vect)
{
	if (NULL != callback) {
		callback();
	} else {
		;
	}
}

