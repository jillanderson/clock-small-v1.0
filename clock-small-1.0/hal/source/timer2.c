#include "../timer2.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stddef.h>

#include "../../common/general/bits.h"
#include "../../common/list/list.h"

static void (*callback)(void);

void hal_timer2_start(uint8_t prescale, uint8_t top)
{
	BIT_MASK_OUT(TCCR2, (BIT_VALUE(WGM21) | BIT_FIELD(prescale,3U,0U)));
	BIT_MASK_OUT(TCNT0, 0U);
	BIT_MASK_OUT(OCR2, top);
	BIT_SET(TIMSK, OCIE2);
}

void hal_timer2_stop(void)
{
	BIT_MASK_OUT(TCCR2, 0U);
	BIT_MASK_OUT(OCR2, 0U);
	BIT_CLEAR(TIMSK, OCIE2);
	/* flagę przerwania kasujemy przez wpisanie jedynki */
	BIT_SET(TIFR, OCF2);
}

void hal_timer2_add_compare_handler(void (*handler)(void))
{
	callback = handler;
}

ISR(TIMER2_COMP_vect)
{
	if (NULL != callback) {
		callback();
	} else {
		;
	}
}

