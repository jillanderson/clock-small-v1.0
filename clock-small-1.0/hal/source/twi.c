#include "../twi.h"

#include <avr/io.h>
#include <stdint.h>
#include <util/twi.h>

#include "../../common/general/bits.h"

/**
 * \note TWSR - TWI Status Register:
 * 	- TWPS1 TWPS0
 * 	- 0 0 clk T2S /1 (No prescaling)
 * 	- 0 1 clk T2S /4 (From prescaler)
 * 	- 1 0 clk T2S /16 (From prescaler)
 * 	- 1 1 clk T2S /64 (From prescaler)
 *
 */
void hal_twi_init(uint16_t speed_kHz)
{
	/* speed_kHz = TWBR * 4^TWPS */
	speed_kHz = (((F_CPU / 1000U) / speed_kHz) - 16U) / 2U;

	uint8_t prescaler = 0U;

	while (speed_kHz > 0xFFU) {
		prescaler++;
		speed_kHz = speed_kHz / 4U;
	};

	TWCR = (1U << TWEA) | (1U << TWEN);

	TWSR = (TWSR & ~((1U << TWPS1) | (1U << TWPS0))) | prescaler;
	TWBR = speed_kHz;
}

uint8_t hal_twi_send_start_and_select(uint8_t address)
{
	// reset TWI control register
	TWCR = 0U;
	// transmit START condition
	TWCR = (1U << TWINT) | (1U << TWSTA) | (1U << TWEN);
	// wait for end of transmission
	while (!(TWCR & (1U << TWINT))) {
		;
	}
	// check if the start condition was successfully transmitted
	if ((TWSR & 0xF8U) != TW_START) {
		return (1U);
	}
	// load slave address into data register
	TWDR = address;
	// start transmission of address
	TWCR = (1U << TWINT) | (1U << TWEN);
	// wait for end of transmission
	while (!(TWCR & (1U << TWINT))) {
		;
	}
	// check if the device has acknowledged the READ / WRITE mode
	uint8_t twst = TW_STATUS & 0xF8U;

	if ((twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK)) {
		return (1U);
	}

	return (0U);
}

uint8_t hal_twi_send_repeat_start_and_select(uint8_t address)
{
	return (hal_twi_send_start_and_select(address));
}

uint8_t hal_twi_send_byte(uint8_t data)
{
	// load data into data register
	TWDR = data;
	// start transmission of data
	TWCR = (1U << TWINT) | (1U << TWEN);
	// wait for end of transmission
	while (!(TWCR & (1U << TWINT))) {
		;
	}

	if ((TWSR & 0xF8U) != TW_MT_DATA_ACK) {
		return (1U);
	}

	return (0U);
}

uint8_t hal_twi_receive_data_ack(void)
{
	// start TWI module and acknowledge data after reception
	TWCR = (1U << TWINT) | (1U << TWEN) | (1U << TWEA);
	// wait for end of transmission
	while (!(TWCR & (1U << TWINT))) {
		;
	}
	// return received data from TWDR
	return (TWDR);
}

uint8_t hal_twi_receive_data_nack(void)
{
	// start receiving without acknowledging reception
	TWCR = (1U << TWINT) | (1U << TWEN);
	// wait for end of transmission
	while (!(TWCR & (1U << TWINT))) {
		;
	}
	// return received data from TWDR
	return (TWDR);
}

void hal_twi_stop(void)
{
	// transmit STOP condition
	TWCR = (1U << TWINT) | (1U << TWEN) | (1U << TWSTO);
	// wait until stop condition is executed and bus released
	while (TWCR & (1U << TWSTO)) {
		;
	}
}

