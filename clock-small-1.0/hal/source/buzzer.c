#include "../buzzer.h"

#include "../../common/avr/gpio.h"
#include "../gpio.h"

static gpio_handle_t *const buzzer = &((gpio_handle_t){BUZZER_GENERATOR});

void hal_buzzer_generator_turn_on(void)
{
	gpio_pin_out_set_hi(buzzer);
}

void hal_buzzer_generator_turn_off(void)
{
	gpio_pin_out_set_lo(buzzer);
}

void hal_buzzer_generator_toggle(void)
{
	gpio_pin_out_toggle(buzzer);
}

