#pragma once

/** \file ********************************************************************
 *
 * sterowanie brzęczykiem z własnym generatorem
 *
 * - przed użyciem należy wywołać funkcję konfiguracji gpio
 *
 *****************************************************************************/

/**
 * włącza brzęczyk
 *
 */
void hal_buzzer_generator_turn_on(void);

/**
 * wyłącza brzączyk
 *
 */
void hal_buzzer_generator_turn_off(void);

/**
 * przełącza brzęczyk w stan przeciwny
 *
 */
void hal_buzzer_generator_toggle(void);

