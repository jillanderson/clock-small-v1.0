#pragma once

/** \file ********************************************************************
 *
 * konfiguracja pinów IO procesora
 *
 *****************************************************************************/

#include "../common/avr/gpio.h"

/**
 * konfiguracja pinów IO procesora
 *
 * \note segmenty wyświetlaczy i dwukropków - aktywny stan wysoki [HI]
 * \note anody wyświetlaczy i dwukropków - aktywny stan wysoki [HI]
 * \note przyciski - stan aktywny niski [LO]
 *
 */
#define DISP_ANODE_0		GPIO(A,0,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_ANODE_1		GPIO(A,1,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_ANODE_2		GPIO(A,2,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_ANODE_3		GPIO(A,3,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_ANODE_4		GPIO(A,4,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_ANODE_5		GPIO(A,5,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_COLON_ANODE_0	GPIO(A,6,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_COLON_ANODE_1	GPIO(A,7,GPIO_DIR_OUTPUT,GPIO_INIT_LO)

#define BUTTON_0		GPIO(B,0,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define BUTTON_1		GPIO(B,1,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define BUTTON_2		GPIO(B,2,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define BUTTON_3		GPIO(B,3,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define BUTTON_4		GPIO(B,4,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define UNUSED_B5		GPIO(B,5,GPIO_DIR_INPUT,GPIO_INIT_PULLUP)
#define UNUSED_B6		GPIO(B,6,GPIO_DIR_INPUT,GPIO_INIT_PULLUP)
#define UNUSED_B7		GPIO(B,7,GPIO_DIR_INPUT,GPIO_INIT_PULLUP)

#define TWI_SCL			GPIO(C,0,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define TWI_SDA			GPIO(C,1,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define DISP_SEG_F		GPIO(C,2,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_SEG_E		GPIO(C,3,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_SEG_D		GPIO(C,4,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_SEG_C		GPIO(C,5,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_SEG_B		GPIO(C,6,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_SEG_A		GPIO(C,7,GPIO_DIR_OUTPUT,GPIO_INIT_LO)

#define INDICATOR_0		GPIO(D,0,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define INDICATOR_1		GPIO(D,1,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define INFRARED_RECEIVER	GPIO(D,2,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define CLOCK_EXT_INTERRUPT	GPIO(D,3,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define TEMPERATURE_SENSOR	GPIO(D,4,GPIO_DIR_INPUT,GPIO_INIT_HIZ)
#define BUZZER_GENERATOR	GPIO(D,5,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_COLON		GPIO(D,6,GPIO_DIR_OUTPUT,GPIO_INIT_LO)
#define DISP_SEG_G		GPIO(D,7,GPIO_DIR_OUTPUT,GPIO_INIT_LO)

/**
 * konfiguruje i inicjalizuje piny io procesora
 *
 * - musi być wywołana przed użyciem driverów sterujących urządzeniami
 *
 */
void hal_gpio_init(void);

/**
 * sprawdza czy początkowa konfiguracja pinów io procesora została wykonana
 *
 * \return true - porty io zainicjowane, false - w przeciwnym przypadku
 *
 */
bool hal_gpio_init_is_done(void);

