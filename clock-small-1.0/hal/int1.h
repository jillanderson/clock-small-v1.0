#pragma once

/** \file ********************************************************************
 *
 * sterowanie wejściem dla zegara zewnętrznego przewranie INT1, ISR(INT1_vect)
 *
 * - przed użyciem należy wywołać funkcję konfiguracji gpio
 *
 *****************************************************************************/

/**
 * włącza obsługę przerwań
 *
 */
void hal_external_int1_start(void);

/**
 * wyłącza obsługę przerwań
 *
 */
void hal_external_int1_stop(void);

/**
 * rejestruje funkcję zwrotną dla uchwytu przerwania zewnętrznego
 *
 * \param handler	funkcja zwrotna klienta uchwytu przerwania
 *
 */
void hal_external_int1_add_handler(void (*handler)(void));

