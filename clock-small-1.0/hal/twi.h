#pragma once

/** \file ********************************************************************
 *
 * niskopoziomowe sterowanie sprzętowym interfejsem twi master
 *
 *****************************************************************************/

#include <stdint.h>
#include <util/twi.h>

/**
 * inicjalizacja magistrali twi
 *
 * \param speed szybkość magistrali w kHz
 *
 * \note 100kHz - szybkość standardowa
 * 	 400kHz - tryb fast
 * \note należy sprawdzić z jaką szybkościa mogą pracować układy towarzyszące
 *
 */
void hal_twi_init(uint16_t speed_kHz);

/**
 * przesyła warunek startu oraz adres i kierunek transmisji
 *
 * \param address adres urządzenia i kierunek transmisji
 *
 * \return zwraca 0 jeśli urządzenie dostępne, 1 jeśli urządzenie niedostępne
 *
 */
uint8_t hal_twi_send_start_and_select(uint8_t address);

/**
 * przesyła warunek powtórnego startu oraz adres i kierunek transmisji
 *
 * \param address adres urządzenia i kierunek transmisji
 *
 * \return zwraca 0 jeśli urządzenie dostępne, 1 jeśli urządzenie niedostępne
 *
 */
uint8_t hal_twi_send_repeat_start_and_select(uint8_t address);

/**
 * przesyłą bit do urządzenia
 *
 * \param data dana do wysłania na magistralę
 *
 * \return zwraca 0 jeśli operacja została zakończona poprawnie,
 * 	   	  1 w razie niepowodzenia
 *
 */
uint8_t hal_twi_send_byte(uint8_t data);

/**
 * czyta bajt z urządzenia i przesyła żądanie dalszego czytania
 *
 * \return zwraca odczytaną wartość
 *
 */
uint8_t hal_twi_receive_data_ack(void);

/**
 * czyta bajt z urządzenia i przesyła żądanie zakończenia odczytu
 *
 * \return zwraca odczytaną wartość
 *
 */
uint8_t hal_twi_receive_data_nack(void);

/**
 * kończy transfer danych i zwalnia magistralę
 *
 */
void hal_twi_stop(void);

