#pragma once

/** \file ********************************************************************
 *
 * timer2 niskopoziomowe sterowanie w trybie CTC
 *
 *****************************************************************************/

#include <stdint.h>

/**
 * uruchamia timer w trybie CTC i włącza przerwanie TIMER2_COMP_vect
 *
 * TCNT2 - Timer/Counter Control Register:
 * 	- CS22 CS21 CS20
 * 	- 0 0 0 No clock source (Timer/Counter stopped).
 * 	- 0 0 1 clk T2S /1 (No prescaling)
 * 	- 0 1 0 clk T2S /8 (From prescaler)
 * 	- 0 1 1 clk T2S /32 (From prescaler)
 * 	- 1 0 0 clk T2S /64 (From prescaler)
 * 	- 1 0 1 clk T2S /128 (From prescaler)
 * 	- 1 1 0 clk T2S /256 (From prescaler)
 * 	- 1 1 1 clk T2S /1024 (From prescaler)
 *
 * \param prescale	maska bitowa prescalera 0-7
 * \param compare	wartość porównania dla licznika
 *
 */
void hal_timer2_start(uint8_t prescale, uint8_t top);

/**
 * wyłacza timer
 *
 * resetuje ustawienia oraz flagę przerwania
 *
 */
void hal_timer2_stop(void);

/**
 * rejestruje funkcję zwrotną dla uchwytu przerwania timera
 *
 * \param handler	funkcja zwrotna klienta uchwytu przerwania
 *
 */
void hal_timer2_add_compare_handler(void (*handler)(void));

