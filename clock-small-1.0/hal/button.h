#pragma once

/** \file ********************************************************************
 *
 * sprawdzenie stanu przycisku
 *
 * - przycisk w stanie aktywnym (wciśnięty) zwiera pin do masy
 * - przed użyciem należy wywołać funkcję konfiguracji gpio
 *
 *****************************************************************************/

#include <stdbool.h>

typedef enum button_id {
	BUTTON_A,
	BUTTON_B,
	BUTTON_C,
	BUTTON_D,
	BUTTON_E
} button_id_t;

/**
 * sprawdza czy przycisk jest wciśnięty
 *
 * \param id	identyfikator przycisku
 *
 * \return true - przycisk wciśnięty, false - przycisk zwolniony
 *
 */
bool hal_button_is_pressed(button_id_t id);

/**
 * sprawdza czy przycisk jest zwolniony
 *
 * \param id	identyfikator przycisku
 *
 * \return true - przycisk zwolniony, false - przycisk wciśnięty
 *
 */
bool hal_button_is_released(button_id_t id);

