#pragma once

/** \file ********************************************************************
 *
 * sterowanie wskaźnikami led
 *
 * - przed użyciem należy wywołać funkcję konfiguracji gpio
 *
 *****************************************************************************/

#include <stdint.h>

typedef enum indicator_id {
	INDICATOR_GREEN,
	INDICATOR_ORANGE
} indicator_id_t;

/**
 * włącza podany wskaźnik
 *
 * \param id	identyfikator wskaźnika
 *
 */
void hal_indicator_turn_on(indicator_id_t id);

/**
 * wyłącza podany wskaźnik
 *
 * \param id	identyfikator wskaźnika
 *
 */
void hal_indicator_turn_off(indicator_id_t id);

/**
 * przełącza podany wskaźnik w stan przeciwny
 *
 * \param id	identyfikator wskaźnika
 *
 */
void hal_indicator_toggle(indicator_id_t id);

