#pragma once

/** \file ********************************************************************
 *
 * systemowa podstawa czasu
 *
 *****************************************************************************/

#include <stdint.h>

#include "../common/gcc/attributes.h"
#include "../common/list/list.h"

/**
 * klient systemowej podstawy czasu
 *
 */
typedef struct ticker {
	void (*callback)(void);
	ListHead ticker_list_member;
} ticker_t;

/**
 * inicjuje systemową podstawę czasu
 *
 * nie włącza globalnego zezwolenia na przerwania
 *
 * \param tick_time_ms	czas między tyknięciami zegara systemowego w ms
 *
 */
void tick_init(uint8_t tick_time_ms);

/**
 * rejestruje funkcję zwrotną dla klienta podstawy czasu i dodaje klienta
 * do listy obsługiwanych
 *
 * \param ticker	wskaźnik do klienta podstawy czasu
 * \param handler	funkcja zwrotna klienta podstawy czasu
 *
 */
void tick_add_ticker(ticker_t *const ticker, void (*handler)(void)) __NONNULL;

