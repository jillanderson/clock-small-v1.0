#pragma once

/** \file ********************************************************************
 *
 * zarządca zadań
 *
 *****************************************************************************/

#include <stdint.h>

#include "../common/gcc/attributes.h"
#include "../common/list/list.h"

#define ONE_SHOT_TASK		0U

typedef struct task {
	void (*callback)(void *arg);
	void *arg;
	uint8_t interval;
	uint8_t delay;
	volatile uint8_t counter;
	volatile uint8_t status;
	ListHead task_list_member;
} task_t;

/**
 * inicjuje zarządcę zadań
 *
 * przed wywołaniem należy skonfigurowaC systemową podstawę czasu
 *
 */
void scheduler_init(void);

/**
 * dodaje zadanie do listy zarejestrowanych i rejestruje funkcję zwrotną
 *
 * - zarejestrowane zadanie jest w stanie TASK_STOPPED
 * - zadanie może być wywoływane cyklicznie z zadanym interwałem czasu podanym
 *	w parametrze period
 * - jeśli parametr period ma wartość 0 lub symbolicznie ONE_SHOT_TASK zadanie
 * 	wykona się jednokrotnie
 * - w obu przypadkach pierwsze wykonanie zadania nastąpi po upływie czasu
 * 	podanym w parametrze delay
 *
 * \param task		wskaźnik do zadania
 * \param handler	funkcja zwrotna zadania
 * \param arg		wskaźnik do argumentu zadania
 * \param delay		czas opóźnienia pierwszego wykonania zadania
 * \param period	interwał czasu dla cyklicznego wykonywania zadania
 *
 */
void scheduler_add_task(task_t *const task, void (*handler)(void *arg),
		void *arg, uint8_t delay, uint8_t period) __NONNULL_ARG(1,2);

/**
 * uruchamia wykonywanie zadania
 *
 * \param task	wskaźnik do zadania
 *
 */
void scheduler_start_task(task_t *const task);

/**
 * zatrzymuje wykonywanie zadania
 *
 * \param task	wskaźnik do zadania
 *
 */
void scheduler_stop_task(task_t *const task);

/**
 * przetwarza zarejestrowane zadania
 *
 * \note musi być wywoływana w pętli głównej programu
 *
 */
void scheduler_dispatch(void);

