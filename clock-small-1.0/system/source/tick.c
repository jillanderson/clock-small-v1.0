#include "../tick.h"

#include <avr/interrupt.h>
#include <stddef.h>

#include "../../common/gcc/utils.h"
#include "../../common/list/list.h"
#include "../../hal/timer2.h"

static ListHead ticker_list = {0};

static void timer2_compare_handler(void);

void tick_init(uint8_t tick_time_ms)
{
	uint16_t const factor[] = {0U, 1U, 8U, 32U, 64U, 128U, 256U, 1024U};

	uint32_t const ticks = (((uint32_t)(F_CPU / 1000U))
			* (uint8_t)(tick_time_ms));

	uint16_t const factor_top =
			(0U == (ticks % UINT8_MAX)) ?
					((uint16_t)(ticks / UINT8_MAX)) :
					((uint16_t)((ticks / UINT8_MAX) + 1U));

	uint8_t presc_number = 0U;
	uint8_t cmp_value = 0U;

	INIT_LIST_HEAD(&ticker_list);

	while (factor_top <= factor[array_size(factor) - 1U]) {
		if (factor_top > factor[presc_number]) {
			presc_number++;
		} else {
			uint8_t ticks_factor = (uint8_t)((((ticks * 2U)
					/ factor[presc_number]) + 1U) / 2U);
			cmp_value = (ticks_factor - 1U);
			break;
		}
	}

	hal_timer2_start(presc_number, cmp_value);
	hal_timer2_add_compare_handler(timer2_compare_handler);
}

void tick_add_ticker(ticker_t *const ticker, void (*handler)(void))
{
	ticker->callback = handler;

	INIT_LIST_HEAD(&(ticker->ticker_list_member));
	list_add(&(ticker->ticker_list_member), &ticker_list);
}

void timer2_compare_handler(void)
{
	ticker_t *ticker = NULL;

	list_for_each_entry(ticker, &ticker_list, ticker_list_member)
	{
		if (NULL != ticker->callback) {
			ticker->callback();
		} else {
			;
		}
	}
}

