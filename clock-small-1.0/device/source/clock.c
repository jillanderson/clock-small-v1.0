#include "../clock.h"

#include <avr/wdt.h>
#include <avr/sleep.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "../../common/avr/jtag.h"
#include "../../common/avr/interrupt.h"
#include "../../common/buffer/circ_buffer.h"
#include "../../common/fsm/fsm.h"
#include "../../common/gcc/build_bug.h"
#include "../../common/general/numbers.h"
#include "../../common/general/bits.h"

#include "../../drivers/gpio.h"
#include "../../drivers/pcf8583.h"
#include "../../drivers/buzzer.h"
#include "../../drivers/buttons.h"
#include "../../drivers/display.h"
#include "../../drivers/indicator_green.h"
#include "../../drivers/indicator_orange.h"
#include "../../drivers/extint1.h"

#include "../../system/scheduler.h"
#include "../../system/tick.h"

/*
 * parametry konfiguracji
 *
 * \note wartości nie mogą przekraczać zakresu uint8_t
 *
 * */
#define CLOCK_EVENT_BUFFER_SIZE				8U

#define BASE_TICK_MS					5U	// 5ms

#define CLOCK_BASE_TICKS				50U	// 250ms
#define CLOCK_START_DELAY_TICKS				20U	// 5000ms

#define BUTTON_BASE_TICKS				2U	// 10ms
#define BUTTON_DEBOUNCE_TICKS				3U	// 30ms
#define BUTTON_DOUBLE_CLICK_TICKS			25U	// 250ms
#define BUTTON_LONG_START_TICKS				100U	// 1000ms
#define BUTTON_LONG_REPEAT_TICKS			25U	// 250ms

#define BUZZER_BASE_TICKS				5U	// 25ms
#define BUZZER_BEEP_HOT_START_TICKS			4U	// 100ms
#define BUZZER_BEEP_COLD_START_TICKS			4U	// 100ms
#define BUZZER_BEEP_SET_OK_TICKS			4U	// 100ms

#define BUZZER_BEEP_UNLIMITED_NUMBER			0U
#define BUZZER_BEEP_HOT_START_NUMBER			1U
#define BUZZER_BEEP_COLD_START_NUMBER			5U

#define BUZZER_BEEP_SET_ENTRY_NUMBER			1U
#define BUZZER_BEEP_SET_EXIT_NUMBER			2U

#define INDICATOR_BASE_TICKS				10U	// 50ms
#define INDICATOR_BLINK_START_TICKS			5U	// 250ms

#define INDICATOR_BLINK_UNLIMITED_NUMBER		0U

#define DISPLAY_REFRESH_RATE_HZ				100U

#define DISPLAY_BRIGHTNESS_STEP_INITIAL			2U
#define DISPLAY_BRIGHTNESS_DAYTIME_STEP_INIT		3U
#define DISPLAY_BRIGHTNESS_NIGHTTIME_STEP_INIT		1U

#define SET_FAST_YEAR_STEP				10U
#define SET_FAST_MONTH_STEP				5U
#define SET_FAST_DAY_STEP				5U
#define SET_FAST_HOUR_STEP				5U
#define SET_FAST_MINUTE_STEP				10U
#define SET_FAST_SECOND_STEP				10U

typedef struct clock {
	fsm_t super;

	fsm_state_t initial;

	fsm_state_t show_time;
	fsm_state_t show_date;

	fsm_state_t set_year;
	fsm_state_t set_month;
	fsm_state_t set_day;
	fsm_state_t set_hour;
	fsm_state_t set_minute;
	fsm_state_t set_second;

	struct {
		circ_buffer_t buffer;
		uint8_t buffer_data[CLOCK_EVENT_BUFFER_SIZE];
	} events;

	pcf8583_time_t time;

	struct {
		uint8_t daytime_step;
		uint8_t nighttime_step;
	} brightness;

	struct {
		uint8_t counter;
	} delay;

	task_t task;
} clock_t;

static clock_t clock = {0};

enum {
	CLOCK_EVENT_START_DELAY_TIMEOUT = FSM_EVENT_USER,
	CLOCK_EVENT_DISPLAY,
	CLOCK_EVENT_UPDATE,
	CLOCK_EVENT_OK_SET,
	CLOCK_EVENT_NEXT_FAST,
	CLOCK_EVENT_PREVIOUS_FAST,
	CLOCK_EVENT_PLUS,
	CLOCK_EVENT_MINUS,
};

/**
 * kroki poziomów jasności wyświetlacza
 */
brightness_t const brightness_step[] = {BRIGHTNESS_STEP_MINIMUM, BRIGHTNESS_STEP_25_PERCENT, BRIGHTNESS_STEP_50_PERCENT,
		BRIGHTNESS_STEP_75_PERCENT, BRIGHTNESS_STEP_MAXIMUM};

uint8_t const brightness_step_qty = array_size(brightness_step);

/**
 * średnie czasy wschodu (sunrise) i zachodu (sunset) słońca w danym miesiącu
 * przeliczone na liczby wg ((h * 60) + m)
 *
 */
uint16_t const __flash sunrise_in_minutes[] = {454, 410, 354, 336, 279, 255, 275, 321, 371, 421, 416, 457};

uint16_t const __flash sunset_in_minutes[] = {955, 1007, 1069, 1174, 1224, 1256, 1247, 1197, 1129, 1057, 944, 925};

/**
 * liczba dni roku (nieprzestepnego) dla kolejnych miesiecy
 *
 */
uint16_t const __flash days_in_month[] = {31U, 28U, 31U, 30U, 31U, 30U, 31U, 31U, 30U, 31U, 30U, 31U};

/**
 * liczba dni od początku roku (nieprzestepnego) dla kolejnych miesiecy
 *
 */
uint16_t const __flash days_to_month[] = {0U, 31U, 59U, 90U, 120U, 151U, 181U, 212U, 243U, 273U, 304U, 334U};

/**
 * konwertuje czas do liczby całkowitej wygodnej dla porównań
 *
 */
static uint16_t number_of_minutes_from_midnight(uint8_t hour, uint8_t minute);

/**
 * sprawdza czy jest pora dzienna czy nocna
 *
 */
static bool daytime_is_it(uint8_t month, uint8_t hour, uint8_t minute);

/**
 * ustawia jasność wyświetlacza w zależności od pory dnia
 *
 */
static void brightness_time_of_day_set(uint8_t month, uint8_t hour, uint8_t minute);

/**
 * sprawdza czy podany rok jest przestępny
 *
 */
static bool year_is_leap(uint16_t year);

/**
 * zwraca liczbę dni w zadanym miesiącu wskazanego roku
 *
 */
static uint8_t number_of_days(uint16_t year, uint8_t month);

/**
 * wyświetla czas i datę
 *
 */
static void display_time(uint8_t hour, uint8_t colon_hm, uint8_t minute, uint8_t colon_ms, uint8_t second);
static void display_date(uint8_t day, uint8_t colon_dm, uint8_t month, uint8_t colon_my, uint16_t year);

/**
 * funkcje maszyny stanów
 *
 */
static void clock_initial_state_handler(clock_t *const me, fsm_msg_t *msg);

static void clock_show_time_state_handler(clock_t *const me, fsm_msg_t *msg);
static void clock_show_date_state_handler(clock_t *const me, fsm_msg_t *msg);

static void clock_set_year_state_handler(clock_t *const me, fsm_msg_t *msg);
static void clock_set_month_state_handler(clock_t *const me, fsm_msg_t *msg);
static void clock_set_day_state_handler(clock_t *const me, fsm_msg_t *msg);
static void clock_set_hour_state_handler(clock_t *const me, fsm_msg_t *msg);
static void clock_set_minute_state_handler(clock_t *const me, fsm_msg_t *msg);
static void clock_set_second_state_handler(clock_t *const me, fsm_msg_t *msg);

static void clock_ctor(clock_t *const me);
static void clock_dispatch(clock_t *const me);

static bool clock_store_event(clock_t *const me, uint8_t event);
static bool clock_fetch_event(clock_t *const me, uint8_t *event);

static void clock_task_handler(void *arg);

/**
 * funkcje obsługi przycisków
 *
 */
static void button_set_handler(button_event_t event);
static void button_plus_handler(button_event_t event);
static void button_minus_handler(button_event_t event);
static void button_next_handler(button_event_t event);
static void button_previous_handler(button_event_t event);

/**
 * funkcja obsługi przerwania zewnętrznego
 *
 */
static void extint_handler(void);

/*
 * wyłączenie komparatora analogowego
 * zmniejsza pobór mocy w trybie IDLE
 *
 * */
static void analog_comparator_disable(void);

void dev_clock_init(void)
{
	interrupts_disable();

	jtag_interface_disable();

	wdt_enable(WDTO_250MS);

	analog_comparator_disable();
	set_sleep_mode(SLEEP_MODE_IDLE);

	tick_init(BASE_TICK_MS);
	scheduler_init();

	drv_gpio_init();

	drv_display_init(DISPLAY_REFRESH_RATE_HZ, brightness_step[DISPLAY_BRIGHTNESS_STEP_INITIAL]);

	drv_external_interrupt_start();
	drv_external_interrupt_handler(extint_handler);

	drv_pcf8583_init();

	drv_buzzer_init(BUZZER_BASE_TICKS);

	drv_button_init(BUTTON_BASE_TICKS, BUTTON_DEBOUNCE_TICKS, BUTTON_DOUBLE_CLICK_TICKS, BUTTON_LONG_START_TICKS,
			BUTTON_LONG_REPEAT_TICKS);
	drv_button_handler(BUTTON_SET, button_set_handler);
	drv_button_handler(BUTTON_PLUS, button_plus_handler);
	drv_button_handler(BUTTON_MINUS, button_minus_handler);
	drv_button_handler(BUTTON_NEXT, button_next_handler);
	drv_button_handler(BUTTON_PREVIOUS, button_previous_handler);

	drv_indicator_green_init(INDICATOR_BASE_TICKS);
	drv_indicator_orange_init(INDICATOR_BASE_TICKS);

	clock_ctor(&clock);

	fsm_start(CAST_TO_FSM_ADDR(&clock));

	interrupts_enable();
}

void dev_clock_run(void)
{
	scheduler_dispatch();
	clock_dispatch(&clock);
	wdt_reset();
	sleep_mode();
}

uint16_t number_of_minutes_from_midnight(uint8_t hour, uint8_t minute)
{
	return ((uint16_t)((hour * (uint16_t)60U) + minute));
}

bool daytime_is_it(uint8_t month, uint8_t hour, uint8_t minute)
{
	bool daytime = false;
	uint16_t time_in_minutes = number_of_minutes_from_midnight(hour, minute);

	if ((time_in_minutes > sunrise_in_minutes[month - 1U]) && (time_in_minutes < sunset_in_minutes[month - 1U])) {
		daytime = true;
	}

	return (daytime);
}

void brightness_time_of_day_set(uint8_t month, uint8_t hour, uint8_t minute)
{
	if (true == daytime_is_it(month, hour, minute)) {
		drv_display_brightness(brightness_step[(clock.brightness).daytime_step]);
	} else {
		drv_display_brightness(brightness_step[(clock.brightness).nighttime_step]);
	}
}

bool year_is_leap(uint16_t year)
{
	return (((((year % 4U) == 0U) && ((year % 100U) != 0U)) || ((year % 400U) == 0U)) ? true : false);
}

uint8_t number_of_days(uint16_t year, uint8_t month)
{
	uint8_t result = days_in_month[month - 1U];

	if (year_is_leap(year) && (month == 2U)) {
		result++;
	}

	return (result);
}

void display_time(uint8_t hour, uint8_t colon_hm, uint8_t minute, uint8_t colon_ms, uint8_t second)
{
	drv_display_show(HOURS_TENS, (hour / 10U));
	drv_display_show(HOURS_UNITS, (hour % 10U));

	drv_display_show(COLON_HM, colon_hm);

	drv_display_show(MINUTES_TENS, (minute / 10U));
	drv_display_show(MINUTES_UNITS, (minute % 10U));

	drv_display_show(COLON_MS, colon_ms);

	drv_display_show(SECONDS_TENS, (second / 10U));
	drv_display_show(SECONDS_UNITS, (second % 10U));
}

void display_date(uint8_t day, uint8_t colon_dm, uint8_t month, uint8_t colon_my, uint16_t year)
{
	drv_display_show(HOURS_TENS, (day / 10U));
	drv_display_show(HOURS_UNITS, (day % 10U));

	drv_display_show(COLON_HM, colon_dm);

	drv_display_show(MINUTES_TENS, (month / 10U));
	drv_display_show(MINUTES_UNITS, (month % 10U));

	drv_display_show(COLON_MS, colon_my);

	drv_display_show(SECONDS_TENS, (uint8_t)((year % 100U) / 10U));
	drv_display_show(SECONDS_UNITS, (uint8_t)((year % 100U) % 10U));
}

void clock_initial_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_START:
		scheduler_start_task(&(me->task));

		drv_display_show(HOURS_TENS, MINUS);
		drv_display_show(HOURS_UNITS, MINUS);

		drv_display_show(COLON_HM, COLON_OFF);

		drv_display_show(MINUTES_TENS, MINUS);
		drv_display_show(MINUTES_UNITS, MINUS);

		drv_display_show(COLON_MS, COLON_OFF);

		drv_display_show(SECONDS_TENS, MINUS);
		drv_display_show(SECONDS_UNITS, MINUS);

		drv_indicator_green_turn_blink(INDICATOR_BLINK_UNLIMITED_NUMBER, INDICATOR_BLINK_START_TICKS);
		drv_indicator_orange_turn_blink(INDICATOR_BLINK_UNLIMITED_NUMBER, INDICATOR_BLINK_START_TICKS);
		break;

	case CLOCK_EVENT_START_DELAY_TIMEOUT:
		drv_pcf8583_start();
		drv_button_start();

		drv_indicator_green_turn_off();
		drv_indicator_orange_turn_off();

		if (true == drv_pcf8583_is_reset()) {
			drv_buzzer_turn_beep(BUZZER_BEEP_COLD_START_NUMBER, BUZZER_BEEP_COLD_START_TICKS);

			(me->time).year = 2020U;
			(me->time).month = 1U;
			(me->time).day = 1U;
			(me->time).hour = 0U;
			(me->time).minute = 0U;
			(me->time).second = 0U;
			(me->time).hsecond = 0U;

			fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_year)));
		} else {
			drv_buzzer_turn_beep(BUZZER_BEEP_HOT_START_NUMBER, BUZZER_BEEP_HOT_START_TICKS);
			fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->show_time)));
		}

		break;

	default:
		;
		break;
	}
}

void clock_show_time_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_off();
		drv_indicator_orange_turn_off();
		break;

	case CLOCK_EVENT_UPDATE:
		drv_pcf8583_read_time(&(me->time));
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		brightness_time_of_day_set((me->time).month, (me->time).hour, (me->time).minute);
		display_time((me->time).hour, COLON_OFF, (me->time).minute, COLON_OFF, (me->time).second);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_ENTRY_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_year)));
		break;

	case CLOCK_EVENT_PLUS:
		if (true == daytime_is_it((me->time).month, (me->time).hour, (me->time).minute)) {
			if ((brightness_step_qty - 1) > (me->brightness).daytime_step) {
				(me->brightness).daytime_step += 1U;
			}
		} else {
			if ((brightness_step_qty - 1) > (me->brightness).nighttime_step) {
				(me->brightness).nighttime_step += 1U;
			}
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (true == daytime_is_it((me->time).month, (me->time).hour, (me->time).minute)) {
			if (0U < (me->brightness).daytime_step) {
				(me->brightness).daytime_step -= 1U;
			}
		} else {
			if (0U < (me->brightness).nighttime_step) {
				(me->brightness).nighttime_step -= 1U;
			}
		}
		break;

	case CLOCK_EVENT_NEXT_FAST:
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->show_date)));
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->show_date)));
		break;

	default:
		;
		break;
	}
}

void clock_show_date_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_off();
		drv_indicator_orange_turn_off();
		break;

	case CLOCK_EVENT_UPDATE:
		drv_pcf8583_read_time(&(me->time));
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		brightness_time_of_day_set((me->time).month, (me->time).hour, (me->time).minute);
		display_date((me->time).day, COLON_OFF, (me->time).month, COLON_OFF, (me->time).year);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_ENTRY_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_year)));
		break;

	case CLOCK_EVENT_PLUS:
		if (true == daytime_is_it((me->time).month, (me->time).hour, (me->time).minute)) {
			if ((brightness_step_qty - 1) > (me->brightness).daytime_step) {
				(me->brightness).daytime_step += 1U;
			}
		} else {
			if ((brightness_step_qty - 1) > (me->brightness).nighttime_step) {
				(me->brightness).nighttime_step += 1U;
			}
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (true == daytime_is_it((me->time).month, (me->time).hour, (me->time).minute)) {
			if (0U < (me->brightness).daytime_step) {
				(me->brightness).daytime_step -= 1U;
			}
		} else {
			if (0U < (me->brightness).nighttime_step) {
				(me->brightness).nighttime_step -= 1U;
			}
		}
		break;

	case CLOCK_EVENT_NEXT_FAST:
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->show_time)));
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->show_time)));
		break;

	default:
		;
		break;
	}
}

void clock_set_year_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_on();
		drv_indicator_orange_turn_off();
		drv_display_brightness(brightness_step[DISPLAY_BRIGHTNESS_STEP_INITIAL]);
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		display_date((me->time).day, COLON_OFF, (me->time).month, COLON_OFF, (me->time).year);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_ENTRY_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_month)));
		break;

	case CLOCK_EVENT_NEXT_FAST:
		if ((9999U - SET_FAST_YEAR_STEP) > (me->time).year) {
			(me->time).year += SET_FAST_YEAR_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		if ((2000U + SET_FAST_YEAR_STEP) < (me->time).year) {
			(me->time).year -= SET_FAST_YEAR_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PLUS:
		if (9999U > (me->time).year) {
			(me->time).year += 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (2000U < (me->time).year) {
			(me->time).year -= 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	default:
		;
		break;
	}
}

void clock_set_month_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_on();
		drv_indicator_orange_turn_off();
		drv_display_brightness(brightness_step[DISPLAY_BRIGHTNESS_STEP_INITIAL]);
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		display_date((me->time).day, COLON_OFF, (me->time).month, COLON_OFF, (me->time).year);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_ENTRY_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_day)));
		break;

	case CLOCK_EVENT_NEXT_FAST:
		if ((12U - SET_FAST_MONTH_STEP) > (me->time).month) {
			(me->time).month += SET_FAST_MONTH_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		if ((1U + SET_FAST_MONTH_STEP) < (me->time).month) {
			(me->time).month -= SET_FAST_MONTH_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PLUS:
		if (12U > (me->time).month) {
			(me->time).month += 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (1U < (me->time).month) {
			(me->time).month -= 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	default:
		;
		break;
	}
}

void clock_set_day_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_on();
		drv_indicator_orange_turn_off();
		drv_display_brightness(brightness_step[DISPLAY_BRIGHTNESS_STEP_INITIAL]);
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		display_date((me->time).day, COLON_OFF, (me->time).month, COLON_OFF, (me->time).year);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_ENTRY_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_hour)));
		break;

	case CLOCK_EVENT_NEXT_FAST:
		if ((number_of_days((me->time).year, (me->time).month) - SET_FAST_DAY_STEP) > (me->time).day) {
			(me->time).day += SET_FAST_DAY_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		if ((1U + SET_FAST_DAY_STEP) < (me->time).day) {
			(me->time).day -= SET_FAST_DAY_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PLUS:
		if (number_of_days((me->time).year, (me->time).month) > (me->time).day) {
			(me->time).day += 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (1U < (me->time).day) {
			(me->time).day -= 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	default:
		;
		break;
	}
}

void clock_set_hour_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_off();
		drv_indicator_orange_turn_on();
		drv_display_brightness(brightness_step[DISPLAY_BRIGHTNESS_STEP_INITIAL]);
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		display_time((me->time).hour, COLON_OFF, (me->time).minute, COLON_OFF, (me->time).second);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_ENTRY_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_minute)));
		break;

	case CLOCK_EVENT_NEXT_FAST:
		if ((23U - SET_FAST_HOUR_STEP) > (me->time).hour) {
			(me->time).hour += SET_FAST_HOUR_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		if ((0U + SET_FAST_HOUR_STEP) < (me->time).hour) {
			(me->time).hour -= SET_FAST_HOUR_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PLUS:
		if (23U > (me->time).hour) {
			(me->time).hour += 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (0U < (me->time).hour) {
			(me->time).hour -= 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	default:
		;
		break;
	}
}

void clock_set_minute_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_off();
		drv_indicator_orange_turn_on();
		drv_display_brightness(brightness_step[DISPLAY_BRIGHTNESS_STEP_INITIAL]);
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		display_time((me->time).hour, COLON_OFF, (me->time).minute, COLON_OFF, (me->time).second);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_ENTRY_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->set_second)));
		break;

	case CLOCK_EVENT_NEXT_FAST:
		if ((59U - SET_FAST_MINUTE_STEP) > (me->time).minute) {
			(me->time).minute += SET_FAST_MINUTE_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		if ((0U + SET_FAST_MINUTE_STEP) < (me->time).minute) {
			(me->time).minute -= SET_FAST_MINUTE_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PLUS:
		if (59U > (me->time).minute) {
			(me->time).minute += 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (0U < (me->time).minute) {
			(me->time).minute -= 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	default:
		;
		break;
	}
}

void clock_set_second_state_handler(clock_t *const me, fsm_msg_t *msg)
{
	switch (msg->event) {
	case FSM_EVENT_ENTRY:
		drv_indicator_green_turn_off();
		drv_indicator_orange_turn_on();
		drv_display_brightness(brightness_step[DISPLAY_BRIGHTNESS_STEP_INITIAL]);
		clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		break;

	case CLOCK_EVENT_DISPLAY:
		display_time((me->time).hour, COLON_OFF, (me->time).minute, COLON_OFF, (me->time).second);
		break;

	case CLOCK_EVENT_OK_SET:
		drv_buzzer_turn_beep(BUZZER_BEEP_SET_EXIT_NUMBER, BUZZER_BEEP_SET_OK_TICKS);
		drv_pcf8583_write_time(&(me->time));
		fsm_transition(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&(me->show_time)));
		break;

	case CLOCK_EVENT_NEXT_FAST:
		if ((59U - SET_FAST_SECOND_STEP) > (me->time).second) {
			(me->time).second += SET_FAST_SECOND_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PREVIOUS_FAST:
		if ((0U + SET_FAST_SECOND_STEP) < (me->time).second) {
			(me->time).second -= SET_FAST_SECOND_STEP;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_PLUS:
		if (59U > (me->time).second) {
			(me->time).second += 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	case CLOCK_EVENT_MINUS:
		if (0U < (me->time).second) {
			(me->time).second -= 1U;
			clock_store_event(&clock, CLOCK_EVENT_DISPLAY);
		}
		break;

	default:
		;
		break;
	}
}

void clock_ctor(clock_t *const me)
{
	fsm_ctor(CAST_TO_FSM_ADDR(me), CAST_TO_FSM_STATE_ADDR(&me->initial));

	fsm_state_ctor(&(me->initial), CAST_TO_FSM_HANDLER(clock_initial_state_handler));

	fsm_state_ctor(&(me->show_time), CAST_TO_FSM_HANDLER(clock_show_time_state_handler));
	fsm_state_ctor(&(me->show_date), CAST_TO_FSM_HANDLER(clock_show_date_state_handler));

	fsm_state_ctor(&(me->set_year), CAST_TO_FSM_HANDLER(clock_set_year_state_handler));
	fsm_state_ctor(&(me->set_month), CAST_TO_FSM_HANDLER(clock_set_month_state_handler));
	fsm_state_ctor(&(me->set_day), CAST_TO_FSM_HANDLER(clock_set_day_state_handler));
	fsm_state_ctor(&(me->set_hour), CAST_TO_FSM_HANDLER(clock_set_hour_state_handler));
	fsm_state_ctor(&(me->set_minute), CAST_TO_FSM_HANDLER(clock_set_minute_state_handler));
	fsm_state_ctor(&(me->set_second), CAST_TO_FSM_HANDLER(clock_set_second_state_handler));

	BUILD_BUG_ON_NOT_POWER_OF_2(CLOCK_EVENT_BUFFER_SIZE);

	circ_buffer_init(&((me->events).buffer), (me->events).buffer_data, CLOCK_EVENT_BUFFER_SIZE);

	(me->brightness).daytime_step = DISPLAY_BRIGHTNESS_DAYTIME_STEP_INIT;
	(me->brightness).nighttime_step = DISPLAY_BRIGHTNESS_NIGHTTIME_STEP_INIT;

	(me->delay).counter = CLOCK_START_DELAY_TICKS;

	scheduler_add_task(&(me->task), clock_task_handler, me, 0U, CLOCK_BASE_TICKS);
}

void clock_dispatch(clock_t *const me)
{
	uint8_t event = FSM_EVENT_NONE;

	while (true == clock_fetch_event(me, &event)) {
		fsm_dispatch(CAST_TO_FSM_ADDR(me), &((fsm_msg_t) {event}));
	}
}

bool clock_store_event(clock_t *const me, uint8_t event)
{
	bool result = false;

	if (false == circ_buffer_is_full(&((me->events).buffer))) {
		circ_buffer_push_item_in_head(&((me->events).buffer), event);
		result = true;
	}

	return (result);
}

bool clock_fetch_event(clock_t *const me, uint8_t *event)
{
	bool result = false;

	if (false == circ_buffer_is_empty(&((me->events).buffer))) {
		*event = circ_buffer_pop_item_from_tail(&((me->events).buffer));
		result = true;
	}

	return (result);
}

void clock_task_handler(void *arg)
{
	clock_t *clock_arg = (clock_t*)arg;

	if (0U == --((clock_arg->delay).counter)) {
		clock_store_event(clock_arg, CLOCK_EVENT_START_DELAY_TIMEOUT);
		scheduler_stop_task(&(clock_arg->task));
	}
}

void button_set_handler(button_event_t event)
{
	switch (event) {
	case BUTTON_SINGLE_CLICK_EVENT:
		clock_store_event(&clock, CLOCK_EVENT_OK_SET);
		break;

	default:
		;
		break;
	}
}

void button_plus_handler(button_event_t event)
{
	switch (event) {
	case BUTTON_SINGLE_CLICK_EVENT:
		clock_store_event(&clock, CLOCK_EVENT_PLUS);
		break;

	default:
		;
		break;
	}
}

void button_minus_handler(button_event_t event)
{
	switch (event) {
	case BUTTON_SINGLE_CLICK_EVENT:
		clock_store_event(&clock, CLOCK_EVENT_MINUS);
		break;

	default:
		;
		break;
	}
}

void button_next_handler(button_event_t event)
{
	switch (event) {
	case BUTTON_SINGLE_CLICK_EVENT:
		clock_store_event(&clock, CLOCK_EVENT_NEXT_FAST);
		break;

	default:
		;
		break;
	}
}

void button_previous_handler(button_event_t event)
{
	switch (event) {
	case BUTTON_SINGLE_CLICK_EVENT:
		clock_store_event(&clock, CLOCK_EVENT_PREVIOUS_FAST);
		break;

	default:
		;
		break;
	}
}

void extint_handler(void)
{
	clock_store_event(&clock, CLOCK_EVENT_UPDATE);
}

void analog_comparator_disable(void)
{
	BIT_SET(ACSR, ACD);
}

