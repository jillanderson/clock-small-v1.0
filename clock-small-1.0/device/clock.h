#pragma once

/** \file ********************************************************************
 *
 * sterowanie zegarem
 *
 *****************************************************************************/

/**
 * inicjalizacja sterowania
 *
 */
void dev_clock_init(void);

/**
 * główny moduł sterownika urządzenia
 *
 * \note musi być wywoływana w pętli głównej
 *
 */
void dev_clock_run(void);

