#include "../fsm.h"

static fsm_msg_t const fsm_msg[] = {
		{FSM_EVENT_NONE},
		{FSM_EVENT_START},
		{FSM_EVENT_ENTRY},
		{FSM_EVENT_EXIT}};

void fsm_state_ctor(fsm_state_t *const me, fsm_event_handler_t handler)
{
	me->handler = handler;
}

void fsm_ctor(fsm_t *const me, fsm_state_t *const initial_state)
{
	me->state = initial_state;
}

void fsm_msg_ctor(fsm_msg_t *const msg, uint8_t event)
{
	msg->event = event;
}

void fsm_start(fsm_t *const me)
{
	((me->state)->handler)(me, &fsm_msg[FSM_EVENT_START]);
}

void fsm_dispatch(fsm_t *const me, fsm_msg_t const *msg)
{
	((me->state)->handler)(me, msg);
}

void fsm_transition(fsm_t *const me, fsm_state_t *const next_state)
{
	fsm_dispatch(me, &fsm_msg[FSM_EVENT_EXIT]);
	me->state = next_state;
	fsm_dispatch(me, &fsm_msg[FSM_EVENT_ENTRY]);
}

