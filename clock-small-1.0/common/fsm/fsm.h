#pragma once

/** \file ********************************************************************
 *
 * maszyna stanów, framework
 *
 *****************************************************************************/

#include <stdint.h>

#include "../gcc/attributes.h"

/**
 * zdarzenie podstawowej maszyny stanów
 *
 * dla maszyny stanów użytkownika dodatkowe parametry dodajemy przez
 * dziedziczenie tej struktury i zdefiniowanie w nowej strukturze
 * dodatkowych parametrów:
 *
 * @code
 * struct {
 * 	fsm_msg_t msg;
 * 	< dodatkowe parametry >
 * };
 * @endcode
 *
 */
typedef struct fsm_msg {
	uint8_t event;
} fsm_msg_t;

/**
 * podstawowe eventy bazowej maszyny stanów
 *
 * dla maszyny stanów użytkownika dodatkowe eventy
 * dodajemy przez ich zdefiniowanie w typie wyliczeniowym
 * począwszy od FSM_EVENT_USER
 *
 * @code
 * enum user_events {
 * 	USER_EVENT_0 = FSM_EVENT_USER, // pierwszy dozwolony sygnał
 * 	USER_EVENT_1,
 * 	...
 * 	USER_EVENT_N
 * };
 * @endcode
 *
 */
enum fsm_event {
	FSM_EVENT_NONE = 0U,
	FSM_EVENT_START,
	FSM_EVENT_ENTRY,
	FSM_EVENT_EXIT,
	FSM_EVENT_USER /*!< pierwsza dozwolona wartość eventu użytkownika */
};

/**
 * bazowa maszyna stanów
 *
 */
typedef struct fsm fsm_t;

/**
 * handler obsługi zdarzeń
 *
 */
typedef void (*fsm_event_handler_t)(fsm_t *fsm, fsm_msg_t const *msg);

/**
 * stan maszyny
 *
 */
typedef struct fsm_state {
	fsm_event_handler_t handler;
} fsm_state_t;

/**
 * bazowa maszyna stanów
 *
 */
struct fsm {
	fsm_state_t *state;
};

/**
 * rzutuje adres maszyny stanów użytkownika na adres bazowej maszyny stanów
 *
 */
#define CAST_TO_FSM_ADDR(address)	((fsm_t *)(address))

/**
 * rzutuje funkcję handlera obsługi zdarzeń użytkownika na funkcję handlera
 * obsługi zdarzeń bazowej maszyny stanów
 *
 */
#define CAST_TO_FSM_HANDLER(handler)	((fsm_event_handler_t)(handler))

/**
 * rzutuje adres stanu użytkownika na adres stanu bazowej maszyny stanów
 *
 */
#define CAST_TO_FSM_STATE_ADDR(address) ((fsm_state_t *)(address))

/**
 * rzutuje adres zdarzenia użytkownika na adres zdarzenia bazowej maszyny stanów
 *
 */
#define CAST_TO_FSM_MSG_ADDR(address)	((fsm_msg_t *)(address))

/**
 * konstruktor przypisujący handler obsługi do zdefiniowanego stanu
 *
 */
void fsm_state_ctor(fsm_state_t *const me, fsm_event_handler_t handler) __NONNULL;

/**
 * konstruktor przypisujący początkowy stan maszyny
 *
 */
void fsm_ctor(fsm_t *const me, fsm_state_t *const initial_state) __NONNULL;

/**
 * konstruktor przypisujący event do wiadomości
 *
 */
void fsm_msg_ctor(fsm_msg_t *const msg, uint8_t event) __NONNULL;

/**
 * uruchamia maszynę stanów i wykonuje obsługę zdarzenia FSM_EVENT_START
 *
 */
void fsm_start(fsm_t *const me) __NONNULL;

/**
 * przetwarza zdarzenia dla aktualnego stanu
 *
 */
void fsm_dispatch(fsm_t *const me, fsm_msg_t const *msg) __NONNULL;

/**
 * wykonuje przejście maszyny do nowego stanu w sekwencji:
 * - przetwarza zdarzenie od sygnału: FSM_EVENT_EXIT dla aktualnego stanu
 * - zmienia stan na nowy, podany jako argument: state
 * - przetwarza zdarzenie od sygnału: FSM_EVENT_ENTRY dla nowego stanu
 *
 */
void fsm_transition(fsm_t *const me, fsm_state_t *const next_state) __NONNULL;

