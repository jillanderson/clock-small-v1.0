#pragma once

/** \file ********************************************************************
 *
 * sterowanie globalnym zezwoleniem na przerwania
 *
 *****************************************************************************/

#include <avr/builtins.h>
#include <avr/interrupt.h>

#include "../gcc/attributes.h"

/**
 * włącza globalne zezwolenie na przerwania
 *
 */
#if defined (__BUILTIN_AVR_SEI)
static __ALWAYS_INLINE void interrupts_enable(void)
{
	__builtin_avr_sei();
}
#else
static __ALWAYS_INLINE void interrupts_enable(void)
{
	sei();
}
#endif

/**
 * wyłącza globalne zezwolenie na przerwania
 *
 */
#if defined (__BUILTIN_AVR_CLI)
static __ALWAYS_INLINE void interrupts_disable(void)
{
	__builtin_avr_cli();
}
#else
static __ALWAYS_INLINE void interrupts_disable(void)
{
	cli();
}
#endif

