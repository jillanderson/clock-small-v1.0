#pragma once

/** \file ********************************************************************
 *
 * obsługa portów gpio
 *
 *****************************************************************************/

#include <stdbool.h>

#include <avr/io.h>

#include "../gcc/attributes.h"

#include "../general/bits.h"
#include "../general/macros.h"

typedef enum gpio_dir {
	GPIO_DIR_INPUT = 0U,
	GPIO_DIR_OUTPUT = 1U
} gpio_dir_t;

typedef enum gpio_init {
	GPIO_INIT_LO = 0U,
	GPIO_INIT_HI = 1U,
	GPIO_INIT_HIZ = 0U,
	GPIO_INIT_PULLUP = 1U
} gpio_init_t;

typedef struct gpio_handle {
	volatile uint8_t *port;
	volatile uint8_t *ddr;
	volatile uint8_t *pin;
	uint8_t bit;
	gpio_dir_t dir;
	gpio_init_t init;
} gpio_handle_t;

/**
 * definiuje port gpio
 *
 * \param port	A, B, C, ...
 * \param bit	0, 1, 2, 3, 4, 5, 6, 7
 * \param dir	GPIO_DIRECTION_OUTPUT lub GPIO_DIRECTION_INPUT
 * \param init	GPIO_INIT_HI, GPIO_INIT_LO, GPIO_INIT_HIZ,
 * 		GPIO_INIT_TRISTATE, GPIO_INIT_PULLUP, GPIO_INIT_NOPULLUP
 *
 */
#define GPIO(port,bit,dir,init)	&PORT##port, &DDR##port, &PIN##port, bit, dir, init

/**
 * inicjuje uchwyt gpio
 *
 * \param gpio	wskaźnik do uchwytu gpio
 * \param port	adres rejestru PORTx, np. &PORTB
 * \param ddr	adres rejestru DDRx, np. &DDRB
 * \param pin   adres rejestru PINx, np. &PINB
 * \param bit	bit 0, 1, 2, 3, 4, 5, 6, 7
 * \param dir	GPIO_DIRECTION_OUTPUT lub GPIO_DIRECTION_INPUT
 * \param init	GPIO_INIT_HI, GPIO_INIT_LO, GPIO_INIT_HIZ,
 * 		GPIO_INIT_TRISTATE, GPIO_INIT_PULLUP, GPIO_INIT_NOPULLUP
 *
 */
__ALWAYS_INLINE void gpio_create(gpio_handle_t *const gpio,
		volatile uint8_t *port, volatile uint8_t *ddr,
		volatile uint8_t *pin, uint8_t bit, uint8_t dir, uint8_t init)
{
	gpio->port = port;
	gpio->ddr = ddr;
	gpio->pin = pin;
	gpio->bit = bit;
	gpio->dir = dir;
	gpio->init = init;
}

/**
 * konfiguruje pin gpio zdefiniowany w uchwycie
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_pin_init(gpio_handle_t const *const gpio)
{
	if (GPIO_DIR_INPUT == (gpio->dir)) {
		BIT_CLEAR(*(gpio->ddr), gpio->bit);
	} else {
		BIT_SET(*(gpio->ddr), gpio->bit);
	}
	
	if (GPIO_INIT_LO == (gpio->init)) {
		BIT_CLEAR(*(gpio->port), gpio->bit);
	} else {
		BIT_SET(*(gpio->port), gpio->bit);
	}
}

/**
 * ustawia pin wyjściowy w stan wysoki
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_pin_out_set_hi(gpio_handle_t const *const gpio)
{
	BIT_SET(*(gpio->port), gpio->bit);
}

/**
 * ustawia pin wyjściowy w stan niski
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_pin_out_set_lo(gpio_handle_t const *const gpio)
{
	BIT_CLEAR(*(gpio->port), gpio->bit);
}

/**
 * ustawia pin wyjściowy w stan przeciwny
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_pin_out_toggle(gpio_handle_t const *const gpio)
{
	BIT_TOGGLE(*(gpio->port), gpio->bit);
}

/**
 * testuje czy pin wyjściowy jest w stanie wysokim
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 * \return true - pin w stanie wysokim, false - w przeciwnym przypadku
 *
 */
__ALWAYS_INLINE bool gpio_pin_out_is_hi(gpio_handle_t const *const gpio)
{
	return (BIT_IS_SET(*(gpio->port), gpio->bit));
}

/**
 * testuje czy pin wyjściowy jest w stanie niskim
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 * \return true - pin w stanie niskim, false - w przeciwnym przypadku
 *
 */
__ALWAYS_INLINE bool gpio_pin_out_is_lo(gpio_handle_t const *const gpio)
{
	return (BIT_IS_CLEAR(*(gpio->port), gpio->bit));
}

/**
 * testuje czy pin wejściowy jest w stanie wysokim
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 * \return true - pin w stanie wysokim, false - w przeciwnym przypadku
 *
 */
__ALWAYS_INLINE bool gpio_pin_in_is_hi(gpio_handle_t const *const gpio)
{
	return (BIT_IS_SET(*(gpio->pin), gpio->bit));
}

/**
 * testuje czy pin wejściowy jest w stanie niskim
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 * \return true - pin w stanie niskim, false - w przeciwnym przypadku
 *
 */
__ALWAYS_INLINE bool gpio_pin_in_is_lo(gpio_handle_t const *const gpio)
{
	return (BIT_IS_CLEAR(*(gpio->pin), gpio->bit));
}

/**
 * ustawia pin jako wyjście
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_dir_set_out(gpio_handle_t const *const gpio)
{
	BIT_SET(*(gpio->ddr), gpio->bit);
}

/**
 * ustawia pin jako wejście
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_dir_set_in(gpio_handle_t const *const gpio)
{
	BIT_CLEAR(*(gpio->ddr), gpio->bit);
}

/**
 * testuje czy pin jest skonfigurowany jako wyjście
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 * \return true - pin jako wyjście, false - w przeciwnym przypadku
 *
 */
__ALWAYS_INLINE bool gpio_dir_is_out(gpio_handle_t const *const gpio)
{
	return (BIT_IS_SET(*(gpio->ddr), gpio->bit));
}

/**
 * testuje czy pin jest skonfigurowany jako wejście
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 * \return true - pin jako wejście, false - w przeciwnym przypadku
 *
 */
__ALWAYS_INLINE bool gpio_dir_is_in(gpio_handle_t const *const gpio)
{
	return (BIT_IS_CLEAR(*(gpio->ddr), gpio->bit));
}

/**
 * włącza pullup
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_pullup_enable(gpio_handle_t const *const gpio)
{
	BIT_SET(*(gpio->port), gpio->bit);
}

/**
 * wyłącza pullup
 *
 * \param gpio	wskaźnik do uchwytu gpio
 *
 */
__ALWAYS_INLINE void gpio_pullup_disable(gpio_handle_t const *const gpio)
{
	BIT_CLEAR(*(gpio->port), gpio->bit);
}

