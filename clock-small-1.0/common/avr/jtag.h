#pragma once

/** \file ********************************************************************
 *
 * sterowanie interfejsem jtag
 *
 *****************************************************************************/

#include <avr/io.h>

#include <util/atomic.h>

#include "../gcc/attributes.h"

/**
 * wyłącza jtag
 *
 */
#if defined (__AVR_ATmega32__)
	#define JTAG_CONTROL_REGISTER	MCUCSR
	#define JTAG_DISABLE_BIT	JTD
	#define AVR_HAVE_JTAG_INTERFACE
#endif

#if defined (AVR_HAVE_JTAG_INTERFACE)
static __ALWAYS_INLINE void jtag_interface_disable(void)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		JTAG_CONTROL_REGISTER |= (1U << JTAG_DISABLE_BIT);
		JTAG_CONTROL_REGISTER |= (1U << JTAG_DISABLE_BIT);
	}
}
#else
static __ALWAYS_INLINE void jtag_interface_disable(void)
{
	;
}
#endif

