#pragma once

/** \file ********************************************************************
 *
 * obsługa encodera, dekodowanie kodu kwadraturowego
 *
 *****************************************************************************/

#include <stdint.h>

#include "../../common/gcc/attributes.h"

typedef enum encoder_event {
	ENCODER_CW_EVENT,
	ENCODER_CCW_EVENT
} encoder_event_t;

typedef struct encoder {
	void (*callback)(encoder_event_t event);
	uint8_t status;
} encoder_t;

/**
 * inicjalizuje encoder
 *
 * \param encoder	wskaźnik do enkodera
 *
 */
void encoder_init(encoder_t *const encoder) __NONNULL;

/**
 * rejestruje funkcję obsługi zdarzeń
 *
 * \param encoder	wskaźnik do enkodera
 * \param callback	funkcja zwrotna obsługująca wykryte zdarzenia
 *
 */
void encoder_register_callback(encoder_t *const encoder,
		void (*callback)(encoder_event_t event)) __NONNULL_ARG(1);

/**
 * resetuje dekoder zdarzeń encodera
 *
 * \param encoder	wskaźnik do enkodera
 *
 */
void encoder_reset(encoder_t *const encoder);

/**
 * uaktualnia stan dekodera zdarzeń od encodera
 *
 * \param encoder	wskaźnik do enkodera
 * \param code		stan wyjść enkodera w kodzie graya w formacie 0000 00xx
 *
 */
void encoder_decode_event(encoder_t *const encoder, uint8_t code) __NONNULL;

