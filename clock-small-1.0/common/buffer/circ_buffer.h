#pragma once

/** \file ********************************************************************
 *
 * bufor kołowy
 *
 *****************************************************************************/

#include <stdint.h>
#include <stdbool.h>

#include "../gcc/attributes.h"

/**
 * bufor cykliczny
 *
 * wizualizacja indeksów bufora:
 * ~~~
 * [0][1][2][3][4][5][6][7] <-- size 8 (index 0-7)
 *  ^           ^
 * tail        head
 * ----        ----
 * get         put
 * ~~~
 *
 */
typedef struct circ_buffer {
	uint8_t head;
	uint8_t tail;
	uint8_t mask;
	uint8_t *data;
} circ_buffer_t;

/**
 * inicjalizuje bufor
 * - rozmiar bufora, musi być potęgą liczby 2
 * - rozmiar bufora nie może przekraczać maksymalnego rozmiaru indeksu
 *
 * \param buf	wskaźnik do bufora
 * \param data	wskaźnik do tablicy przechowującej dane
 * \param size	rozmiar tablicy przechowującej dane (rozmiar bufora)
 *
 */
void circ_buffer_init(circ_buffer_t *const buf, uint8_t *const data,
		uint8_t size) __NONNULL;

/**
 * oblicza zdeklarowany rozmia bufora
 *
 * \param buf	wskaźnik do bufora
 *
 * \return 	rozmiar bufora
 *
 */
uint8_t circ_buffer_get_size(circ_buffer_t const *const buf) __NONNULL;

/**
 * oblicza ilość elementów przechowywanych aktualnie w buforze
 *
 * \param buf	wskaźnik do bufora
 *
 * \return	ilość elementów w buforze
 *
 */
uint8_t circ_buffer_get_number_of_elements(circ_buffer_t const *const buf) __NONNULL;

/**
 * testuje czy bufor jest pusty
 *
 * \param buf	wskaźnik do bufora
 *
 * \return 	true - bufor jest pusty, false - w przeciwnym przypadku
 *
 */
bool circ_buffer_is_empty(circ_buffer_t const *const buf) __NONNULL;

/**
 * testuje czy bufor jest pełny
 *
 * \param buf	wskaźnik do bufora
 *
 * \return 	true - bufor jest pałny, false - w przeciwnym przypadku
 *
 */
bool circ_buffer_is_full(circ_buffer_t const *const buf) __NONNULL;

/**
 * testuje czy bufor nie jest w stanie overflowed lub underflowed
 *
 * \param buf	wskaźnik do bufora
 *
 * \return 	true - bufor w stanie niedozwolonym overflowed lub underflowed,
 * 		false - w przeciwnym przypadku
 *
 */
bool circ_buffer_is_error(circ_buffer_t const *const buf) __NONNULL;

/**
 * czyści bufor
 *
 * \param buf	wskaźnik do bufora
 *
 */
void circ_buffer_reset_data(circ_buffer_t *const buf) __NONNULL;

/**
 * dodaje kolejny element
 * - tryb standardowy
 * - dodanie elementu na pozycji head bufora
 * - nadpisuje elementy przy przekroczeniu rozmiaru bufora
 *
 * \param buf	wskaźnik do bufora
 * \param data	wartość elementu
 *
 */
void circ_buffer_push_item_in_head(circ_buffer_t *const buf, uint8_t data) __NONNULL;

/**
 * dodaje element przed pierwszym elementem
 * - tryb niestandardowy, dodany element zostanie odczytany jako pierwszy, może
 *   służyć np. do dodawania eventów o wysokim priorytecie, które mają zostać
 *   odczytane natychmiast z pominięciem pozostałych elementów
 * - dodanie elementu na pozycji tail bufora
 * - nadpisuje elementy przy przekroczeniu rozmiaru bufora
 *
 * \param buf		wskaźnik do bufora
 * \param data		wartość elementu
 *
 */
void circ_buffer_push_item_in_tail(circ_buffer_t *const buf, uint8_t data) __NONNULL;

/**
 * odczytuje element nr za pierwszym elementem
 * - odczyt z pozycji (tail + nr)
 * - nie zmienia znaczników zapisu i odczytu
 * - nie usuwa elementu z bufora
 *
 * \param buf	wskaźnik do bufora
 * \param nr	numer elementu za pierwszym elementem
 *
 * \return	zwraca przechowywanę wartość
 *
 */
uint8_t circ_buffer_pop_item_nr_from_tail(circ_buffer_t const *const buf,
		uint8_t nr) __NONNULL;

/**
 * odczytuje pierwszy element
 * - odczyt z pozycji tail bufora
 * - zwiększa znacznik tail
 *
 * \param buf	wskaźnik do bufora
 *
 * \return	zwraca przechowywanę wartość
 *
 */
uint8_t circ_buffer_pop_item_from_tail(circ_buffer_t *const buf) __NONNULL;

/**
 * odczytuje element nr przed ostatnim
 * - odczyt z pozycji (head - nr)
 * - nie zmienia znaczników zapisu i odczytu
 * - nie usuwa elementu z bufora
 *
 * \param buf	wskaźnik do bufora
 * \param nr	numer elementu przed elementem ostatnim
 *
 * \return	zwraca przechowywanę wartość
 *
 */
uint8_t circ_buffer_pop_item_nr_from_head(circ_buffer_t const *const buf,
		uint8_t nr) __NONNULL;

/**
 * odczytuje ostatni element
 * - odczyt z pozycji (head - 1)
 * - zmniejsza znacznik head
 *
 * \param buf	wskaźnik do bufora
 *
 * \return	zwraca przechowywanę wartość
 *
 */
uint8_t circ_buffer_pop_item_from_head(circ_buffer_t *const buf) __NONNULL;

