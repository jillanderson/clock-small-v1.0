#pragma once

/** \file ********************************************************************
 *
 * struktury danych zapewniające dostęp do poszczególnych bitów rejestrów sfr
 * (special function registers)
 *
 *****************************************************************************/

#include <stdint.h>

#include "../gcc/attributes.h"

struct __bitfield {
	uint8_t b0 :1;
	uint8_t b1 :1;
	uint8_t b2 :1;
	uint8_t b3 :1;
	uint8_t b4 :1;
	uint8_t b5 :1;
	uint8_t b6 :1;
	uint8_t b7 :1;
} __PACKED;

typedef union __bitfield_sfr bitfield_sfr_t;

union __bitfield_sfr {
	uint8_t byte;
	struct __bitfield bit;
};

#define __STRUCT_BITFIELD_NAMED(__a, __b, __c, __d, __e, __f, __g, __h)	\
	struct {							\
		uint8_t __a : 1;					\
		uint8_t __b : 1;					\
		uint8_t __c : 1;					\
		uint8_t __d : 1;					\
		uint8_t __e : 1;					\
		uint8_t __f : 1;					\
		uint8_t __g : 1;					\
		uint8_t __h : 1;					\
	} __PACKED

#define __BITFIELD_SFR(__name, __a, __b, __c, __d, __e, __f, __g, __h)		\
	union __name {								\
		uint8_t byte;							\
		__STRUCT_BITFIELD_NAMED(__a, __b, __c, __d, __e, __f, __g, __h);\
	}

#define BITFIELD_SFR(__name, __addr, __a, __b, __c, __d, __e, __f, __g, __h)	\
	volatile __BITFIELD_SFR(__name, __a, __b, __c, __d, __e, __f, __g, __h)	\
	__name __IO(__addr)

