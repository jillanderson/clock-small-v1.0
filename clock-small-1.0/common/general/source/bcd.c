#include "../bcd.h"

#include <stdint.h>

uint16_t __bcd2bin(uint8_t val)
{
	return ((val & 0x0FU) + (val >> 4U) * 10U);
}

uint8_t __bin2bcd(uint16_t val)
{
	return (((val / 10U) << 4U) + val % 10U);
}

