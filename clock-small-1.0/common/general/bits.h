#pragma once

/** \file ********************************************************************
 *
 * makra manipulujące bitami, maskami i polami bitowymi
 *
 *****************************************************************************/

#include <stdint.h>

/** \name bit
 */
/**\{*/
#define BIT_VALUE(nr)			(1U << (nr))
#define BIT_SET(var,nr)			((var) |= BIT_VALUE((nr)))
#define BIT_CLEAR(var,nr)		((var) &= (~BIT_VALUE((nr))))
#define BIT_TOGGLE(var,nr)		((var) ^= BIT_VALUE((nr)))
#define BIT_WRITE(val,var,nr)		((val) ? BIT_SET((var),(nr)) : \
					 BIT_CLEAR((var),(nr)))
#define BIT_CHECK(var,nr)		((var) & BIT_VALUE((nr)))
#define BIT_VALUE_CHECK(var,nr)		(((var) >> (nr)) & 1U)
#define BIT_IS_SET(var,nr)		(BIT_VALUE((nr)) == BIT_CHECK((var),(nr)))
#define BIT_IS_CLEAR(var,nr)		(0U == BIT_CHECK((var),(nr)))
#define BIT_WAIT_UNTIL_IS_SET(var,nr)	({ do {;} while (BIT_IS_CLEAR((var),(bit))); })
#define BIT_WAIT_UNTIL_IS_CLEAR(var,nr)	({ do {;} while (PX_BIT_IS_HI((var),(bit))); })
/**\}*/

/** \name bit_mask
 */
/**\{*/
#define BIT_MASK_RANGE(msb,lsb)			(((1U << ((msb) + 1U)) - 1U) & \
							(~((1U << (lsb)) - 1U)))
#define BIT_MASK_NBITS(msb,lsb)		 	((msb) - (lsb) + 1U)
#define BIT_MASK_LENGTH(len)		 	(BIT_VALUE((len)) - 1U)
#define BIT_MASK_LENGTH_SHIFTED(len,lsb) 	(BIT_MASK_LENGTH((len)) << (lsb))
#define BIT_MASK_OUT(var,mask)			((var) = (mask))
#define BIT_MASK_SET(var,mask)			((var) |= (mask))
#define BIT_MASK_SET_SHIFTED(var,mask,shift)	((var) |= ((mask)<<(shift)))
#define BIT_MASK_CLEAR(var,mask)		((var) &= (~(mask)))
#define BIT_MASK_CLEAR_SHIFTED(var,mask,shift)	((var) &= ~((mask)<<(shift)))
#define BIT_MASK_TOGGLE(var,mask)		((var) ^= (mask))
#define BIT_MASK_CHECK(var,mask)		((var) & (mask))
#define BIT_MASK_IS_SET(var,mask)		((mask) == BIT_MASK_CHECK((var),(mask)))
#define BIT_MASK_IS_CLEAR(var,mask)		(0U == BIT_MASK_CHECK((var),(mask)))
/**\}*/

/** \name bit_field
 */
/**\{*/
#define BIT_FIELD(val,len,lsb)		(((val) & BIT_MASK_LENGTH((len))) << (lsb))
#define BIT_FIELD_GET(var,len,lsb)	(((var) >> (lsb)) & BIT_MASK_LENGTH((len)))
#define BIT_FIELD_SET(var,val,len,lsb)	({ \
	do { \
		((var) = (((var) & (~BIT_MASK_LENGTH_SHIFTED((len),(lsb)))) | \
				BIT_FIELD((val),(len),(lsb)))); \
	} while (0); })
/**\}*/

/**
 * łączenie 8 x 1 bitów w wartość 8 bitową
 *
 */
#define U8_CONCAT_U1(b7, b6, b5, b4, b3, b2, b1, b0)	(((b7)<<7U) | ((b6)<<6U) | \
							 ((b5)<<5U) | ((b4)<<4U) | \
							 ((b3)<<3U) | ((b2)<<2U) | \
							 ((b1)<<1U) | ((b0)<<0U))

/**
 * łączenie 2 x 4 bitów w wartość 8 bitową
 *
 */
#define U8_CONCAT_U4(hi4, lo4)	((((uint8_t)((hi4) & 0x0fU))<<4U) | \
				 ((uint8_t)((lo4) & 0x0fU)))

/**
 * łączenie 2 x 8 bitów w wartość 16 bitową
 *
 */
#define U16_CONCAT_U8(hi8, lo8)	((((uint16_t)(hi8))<<8U) | ((uint16_t)(lo8)))

/**
 * łączenie 2 x 8bitów w wartość 32 bitową
 *
 */
#define U32_CONCAT_U8(hi8, mh8, ml8, lo8)	((((uint32_t)(hi8))<<24U) | \
						 (((uint32_t)(mh8))<<16U) | \
						 (((uint32_t)(ml8))<<8U ) | \
						 ((uint32_t)(lo8)))

/**
 * wyciągnięcie 4 najstarszych bitów z watrości 8 bitowej z wyrównaniem do prawej
 *
 */
#define U8_HI4(data)		((uint8_t)(((data)>>4) & 0x0fU))

/**
 * wyciągnięcie 4 najmłodszych bitów z watrości 8 bitowej z wyrównaniem do prawej
 *
 */
#define U8_LO4(data)		((uint8_t)((data) & 0x0fU))

/**
 * wyciągnięcie 8 najstarszych bitów z watrości 16 bitowej z wyrównaniem do prawej
 *
 */
#define U16_HI8(data)		((uint8_t)(((data)>>8U) & 0xffU))

/**
 * wyciągnięcie 8 najmłodszych bitów z watrości 16 bitowej z wyrównaniem do prawej
 *
 */
#define U16_LO8(data)		((uint8_t)((data) & 0xffU))

/**
 * wyciągnięcie 8 najstarszych bitów (31 ... 24) z watrości 32 bitowej
 * z wyrównaniem do prawej
 *
 */
#define U32_HI8(data)		((uint8_t)(((data)>>24U) & 0xffU))

/**
 * wyciągnięcie 8 wewnętrznych starszych bitów (23 ... 16) z watrości 32 bitowej
 * z wyrównaniem do prawej
 *
 */
#define U32_MH8(data)		((uint8_t)(((data)>>16U) & 0xffU))

/**
 * wyciągnięcie 8 wewnętrznych młodszych bitów (15 ... 8) z watrości 32 bitowej
 * z wyrównaniem do prawej
 *
 */
#define U32_ML8(data)		((uint8_t)(((data)>>8U) & 0xffU))

/**
 * wyciągnięcie 8 najmłodszych bitów (7 ... 0) z watrości 32 bitowej
 * z wyrównaniem do prawej
 *
 */
#define U32_LO8(data)		((uint8_t)((data) & 0xffU))

/**
 * wyciągnięcie 16 najstarszych bitów (31 ... 16) z watrości 32 bitowej
 * z wyrównaniem do prawej
 *
 */
#define U32_HI16(data)		((uint16_t)(((data)>>16U) & 0xffffU))

/**
 * wyciągnięcie 16 najmłodszych bitów (15 ... 0) z watrości 32 bitowej
 * z wyrównaniem do prawej
 *
 */
#define U32_LO16(data)		((uint16_t)((data) & 0xffffU))

