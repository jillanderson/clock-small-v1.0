#pragma once

/** \file ********************************************************************
 *
 * wspólne definicje kierunków, stanów, poziomów itp.
 *
 *****************************************************************************/

/** \name data_direction
 *
 * kierunki przepływu danych
 *
 */
/**\{*/
#define READ			0U
#define WRITE			1U
/**\}*/

/** \name logic_level
 *
 * poziomy stanów logicznych
 *
 */
/**\{*/
#define	LOW			0U
#define HIGH			1U
/**\}*/

/** \name execute_status
 *
 * statusy wykonania funkcji i statusy wyrażeń
 *
 */
/**\{*/
#define	SUCCESS			0U
#define FAILURE			1U
#define	FAILURE_USER_DEF	2U
/**\}*/

