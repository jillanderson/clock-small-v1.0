#pragma once


/** \file ********************************************************************
 *
 * makra bsługujące konwersje BCD
 *
 *****************************************************************************/

/** \note Built-in Function: int __builtin_constant_p (exp)
 *
 * \note You can use the built-in function __builtin_constant_p to determine if
 * a value is known to be constant at compile time and hence that GCC can
 * perform constant-folding on expressions involving that value. The argument
 * of the function is the value to test. The function returns the integer 1
 * if the argument is known to be a compile-time constant and 0 if it is not
 * known to be a compile-time constant. A return of 0 does not indicate that
 * the value is not a constant, but merely that GCC cannot prove it is
 * a constant with the specified value of the -O option.
 *
 * \note You typically use this function in an embedded application where memory is
 * a critical resource. If you have some complex calculation, you may want it
 * to be folded if it involves constants, but need to call a function if
 * it does not.
 *
 */

#include <stdint.h>

#define bcd2bin(x)				\
	(__builtin_constant_p((uint8_t)(x)) ?	\
	 const_bcd2bin(x) :			\
	 __bcd2bin(x))

#define bin2bcd(x)				\
	(__builtin_constant_p((uint8_t)(x)) ?	\
	 const_bin2bcd(x) :			\
	 __bin2bcd(x))

#define const_bcd2bin(x)	(((x) & 0x0FU) + ((x) >> 4U) * 10U)
#define const_bin2bcd(x)	((((x) / 10U) << 4U) + (x) % 10U)

uint16_t __bcd2bin(uint8_t val) __attribute__((const));
uint8_t __bin2bcd(uint16_t val) __attribute__((const));

