#pragma once

/** \file ********************************************************************
 *
 * makra pomocnicze
 *
 *****************************************************************************/

/** \name expand_combo
 *
 * rozwija wyrażenie złożone
 *
 */
/**\{*/
#if !defined (EXPAND_COMBO)
#define	EXPAND_COMBO(combo)	combo
#endif
/**\}*/

/** \name stringify
 *
 * konwertuje parametr a na string
 *
 */
/**\{*/
#if !defined (STRINGIFY)
#define	STRINGIFY(a)		#a
#endif
/**\}*/

/** \name stringify_indirect
 *
 * Indirect stringification
 *
 * Doing two levels allows the parameter to be a
 * macro itself.  For example, compile with -DFOO=bar, __stringify(FOO)
 * converts to "bar"
 *
 */
/**\{*/
#if !defined (STRINGIFY_INDIRECT)
#define __STRINGIFY_INDIRECT__(x...)	#x
#define STRINGIFY_INDIRECT(x...)	__STRINGIFY_INDIRECT__(x)
#endif
/**\}*/

/** \name concatenation
 *
 *skleja makra i nazwy
 *
 */
/**\{*/
#if !defined (PASTE)
#define __PASTE__(a,b)		a##b
#define PASTE(a,b)		__PASTE__(a,b)
#endif

#if !defined (EVALUATE)
#define __EVALUATE__(a,b)	a##b
#define EVALUATE(a,b)		__EVALUATE__(a,b)
#endif

#if !defined (CONCAT)
#define __CONCATENATE__(a,b)	a##b
#define CONCAT(a,b)		__CONCATENATE__(a,b)
#endif
/**\}*/

/** \name unique_id
 *
 * generuje unikalny identyfikator
 *
 * \__COUNTER__, predefiniowane makro gcc
 *
 */
/**\{*/
#if !defined (UNIQUE_ID)
#define UNIQUE_ID(prefix) PASTE(PASTE(__UNIQUE_ID, prefix), __COUNTER__)
#endif
/**\}*/

/** \name not_quite_unique_id
 *
 * generuje niecałkiem unikalny identyfikator
 *
 * \__LINE__, predefiniowane makro gcc
 *
 */
/**\{*/
#if !defined (NOT_QUITE_UNIQUE_ID)
#define NOT_QUITE_UNIQUE_ID(prefix) PASTE(PASTE(__NOT_QUITE_UNIQUE_ID, prefix), __LINE__)
#endif
/**\}*/

/** \name info_generator
 *
 * generuje komunikaty użytkownika
 *
 * komunikaty: info, msg należy pisać bez cudzysłowów
 *
 */
/**\{*/
#define __PRAGMA_INFO(info, msg) \
	_Pragma(STRINGIFY(message(__FILE__ " (" STRINGIFY_INDIRECT(__LINE__) \
			")" ": " STRINGIFY(info) ": " STRINGIFY(msg))))

#define SHOW_MESSAGE(msg)	__PRAGMA_INFO([message], msg)
#define SHOW_WARNING(msg)	__PRAGMA_INFO([warning], msg)
#define SHOW_ERROR(msg)		__PRAGMA_INFO([error], msg)
/**\}*/

