#pragma once

/** \file ********************************************************************
 *
 * makra wspomagające obliczenia numeryczne
 *
 *****************************************************************************/

/**
 * zwraca mniejszą z zadanych wartości
 *
 * sprawdza czy typy x i y są zgodne
 *
 */
#define MIN_NUMBER(x, y) ({ \
		typeof(x) _min_x = (x); \
		typeof(y) _min_y = (y); \
		(void) (&_min_x == &_min_y); \
		(_min_x < _min_y) ? _min_x : _min_y; })

/**
 * zwraca większą z zadanych wartości
 *
 * sprawdza czy typy x i y są zgodne
 *
 */
#define MAX_NUMBER(x, y) ({ \
		typeof(x) _max_x = (x); \
		typeof(y) _max_y = (y); \
		(void) (&_max_x == &_max_y); \
		(_max_x > _max_y) ? _max_x : _max_y; })

/**
 * przycina zadaną liczbę x do zakresu <lo,hi>
 *
 * sprawdza czy typy x i y są zgodne
 *
 * \return
 * 	- x 	jeśli x>=lo and x<=hi
 * 	- lo 	jeśli x<lo
 * 	- hi 	jeśli x>hi
 *
 */
#define CLAMP_NUMBER(x, lo, hi) \
	MIN_NUMBER(((typeof(x))MAX_NUMBER((x),(lo))),(hi))

/**
 * porównuje zadane wartości x i y
 *
 * sprawdza czy typy x i y są zgodne
 *
 * \return
 *  	- 0	jeśli x=y
 *  	- 1	jeśli x>y
 *  	- 2	jeśli x<y
 *
 */
#define COMPARE_NUMBERS(x, y) ({ \
		typeof(x) _cmp_x = (x); \
		typeof(y) _cmp_y = (y); \
		(void) (&_cmp_x == &_cmp_y); \
		(_cmp_x == _cmp_y) ? 0U : (_cmp_x > _cmp_y) ? 1U : 2U; })

/**
 * sprawdza czy zadana liczba x zawiera się w zakresie <lo,hi>
 *
 * sprawdza czy typy x, lo, hi są zgodne
 *
 * \return
 *  	- 0 	jeśli x>=lo and x<=hi
 *  	- 1	jeśli x>hi
 *  	- 2	jeśli x<lo
 *
 */
#define BETWEEN_NUMBERS(x, lo, hi) ({ \
		typeof(x) _cmp_x = (x); \
		typeof(lo) _cmp_lo = (lo); \
		typeof(hi) _cmp_hi = (hi); \
		(void)(&_cmp_x == &_cmp_lo); \
		(void)(&_cmp_x == &_cmp_hi); \
		(_cmp_x > _cmp_hi) ? 1U : (_cmp_x < _cmp_lo) ? 2U : 0U; })

/** \addtogroup testing_in_type
 * testuje liczby zadanego typu, używa wartości tymczasowych zadanego typu
 * niezależnie od typu liczb będących argumentami.
 *
 * jest to użyteczne kiedy np. val jest typu unsigned a mn i max są literałami
 * które inaczej zostałyby rozwinięte do typu signed int. (promocja do int)
 *
 */
/**\{*/
#define MIN_NUMBER_TYPE(type, x, y) ({ \
		type __min1 = (x); \
		type __min2 = (y); \
		__min1 < __min2 ? __min1: __min2; })

#define MAX_NUMBER_TYPE(type, x, y) ({ \
		type __max1 = (x); \
		type __max2 = (y); \
		__max1 > __max2 ? __max1: __max2; })

#define CLAMP_NUMBER_TYPE(type, val, lo, hi) \
	MIN_NUMBER_TYPE(type, MAX_NUMBER_TYPE(type, val, lo), hi)

#define CLAMP_VAL(val, lo, hi)	CLAMP_NUMBER_TYPE(typeof(val), val, lo, hi)
/**\}*/

/**
 * zamienia miejscami wartości x i y
 *
 */
#define SWAP_NUMBERS(x, y) ({ \
		do { \
			typeof(x) _tmp = (x); \
			(x) = (y); \
			(y) = _tmp; \
		} while (0U); })

/**
 * makro pomocnicze przy obliczaniu zaokrągleń
 *
 */
#define __ROUND_MASK(x, y) \
		((typeof(x))((y) - 1))

/**
 * zaokrągla x w górę do mastępnej wielokrotności y kóra musi być potęgą 2
 *
 * x - wartość do zaokrąglenia
 * y - wielokrotność do zaokrąglenia w górę, musi być potęgą 2
 *
 */
#define ROUND_UP_POWER_OF_TWO(x, y) \
		((((x) - 1) | __ROUND_MASK(x, y)) + 1)

/**
 * zaokrągla x w dół do mastępnej wielokrotności y kóra musi być potęgą 2
 *
 * x - wartość do zaokrąglenia
 * y - wielokrotność do zaokrąglenia w dół, musi być potęgą 2
 *
 */
#define ROUND_DOWN_POWER_OF_TWO(x, y) \
		((x) & ~__ROUND_MASK(x, y))

/**
 * zaokrągla x w górę do mastępnej wielokrotności y
 *
 * x - wartość do zaokrąglenia
 * y - wielokrotność do zaokrąglenia w górę
 *
 */
#define ROUND_UP(x, y) ({ \
		typeof(y) __y = (y); \
		(((x) + (__y - 1)) / __y) * __y; })

/**
 * zaokrągla x w dół do mastępnej wielokrotności y
 *
 * x - wartość do zaokrąglenia
 * y - wielokrotność do zaokrąglenia w dół
 *
 */
#define ROUND_DOWN(x, y) ({ \
		typeof(x) __x = (x); \
		__x - (__x % (y)); })

/**
 * dzieli dodatnią lub ujemną dzielną n przez dodatni lub ujemny dzielnik d
 * i zaokrągla wynik w górę do najbliższej liczby całkowitej.
 *
 * \warning rezultat jest niezdefiniowany dla ujemnego dzielnika d
 * jeśli dzielna n jest typu unsigned i dla ujemnej dzielnej n jeśli dzielnik d
 * jest typu unsigned.
 *
 */
#define DIV_ROUND_UP(n, d) ({ \
		const typeof(d) _d = (d); \
		(((n) + (_d) - 1) / (_d)); })

/**
 * dzieli dodatnią lub ujemną dzielną n przez dodatni lub ujemny dzielnik d
 * i zaokrągla wynik do najbliższej liczby całkowitej.
 *
 * \warning rezultat jest niezdefiniowany dla ujemnego dzielnika d
 * jeśli dzielna n jest typu unsigned i dla ujemnej dzielnej n jeśli dzielnik d
 * jest typu unsigned.
 *
 */
#define DIV_ROUND_CLOSEST(n, d) ({ \
			typeof(n) _n = (n); \
			typeof(d) _d = (d); \
			(((typeof(n))(-1)) > 0 || \
			 ((typeof(d))(-1)) > 0 || \
			 (((_n) > 0) == ((_d) > 0))) ? \
				(((_n) + ((_d) / 2)) / (_d)) : \
				(((_n) - ((_d) / 2)) / (_d)); })

/**
 * dzieli modulo dodatnią dzielną n przez dodatni dzielnik d
 *
 */
#define DIV_MODULO(n, d)	((n) - (((n) / (d)) * (d)))

/**
 * sprawdza czy x jest parzyste
 *
 */
#define NUMBER_IS_EVEN(x)	(0U == ((x) & 1U))

/**
 * sprawdza czy x jest nieparzyste
 *
 */
#define NUMBER_IS_ODD(x)	(1U == ((x) & 1U))

/**
 * sprawdza czy x jest potęgą 2. (2^n)
 *
 */
#define NUMBER_IS_POWER_OF_TWO(x) \
	((0U != (x)) && (0U == ((x) & ((x) - 1U))))

