#pragma once

/** \file ********************************************************************
 *
 * dekoder zdarzeń przycisku
 *
 *****************************************************************************/

#include <stdbool.h>
#include <stdint.h>

#include "../gcc/attributes.h"

typedef enum button_event {
	BUTTON_PRESS_DOWN_EVENT,
	BUTTON_PRESS_UP_EVENT,
	BUTTON_SINGLE_CLICK_EVENT,
	BUTTON_DOUBLE_CLICK_EVENT,
	BUTTON_LONG_PRESS_START_EVENT,
	BUTTON_LONG_PRESS_HOLD_EVENT,
	BUTTON_LONG_PRESS_STOP_EVENT
} button_event_t;

typedef struct button {
	uint8_t debounce_tick_nr;
	uint8_t double_click_ticks_nr;
	uint8_t long_start_ticks_nr;
	uint8_t long_repeat_ticks_nr;
	uint8_t tick_cnt;
	uint8_t long_repeat_tick_cnt;
	uint8_t debounce_cnt;
	bool is_pressed;
	uint8_t state;
	void (*callback)(button_event_t event);
} button_t;

/**
 * inicjuje dekoder
 *
 * \param button		wskaźnik do przycisku
 * \param debounce_tick_nr	czas debouncingu styków będący krotnością
 * 				 wewnętrznej podstawy czasu
 * \param double_click_ticks_nr	czas oczekiwania na dwuklik będący krotnością
 * 				 wewnętrznej podstawy czasu
 * \param long_start_ticks_nr	czas do wykrycia długiego wciśnięcia będący
 * 				 krotnością wewnętrznej podstawy czasu
 * \param long_repeat_ticks_nr	czas repetycji przy długim wciśnięciu będący
 * 				 krotnością wewnętrznej podstawy czasu
 *
 */
void button_init(button_t *const button, uint8_t debounce_tick_nr,
		uint8_t double_click_ticks_nr, uint8_t long_start_ticks_nr,
		uint8_t long_repeat_ticks_nr) __NONNULL;

/**
 * rejestruje funkcję zwrotną
 *
 * \param button	wskaźnik do przycisku
 * \param handler	wskaźnik do funkcji zwrotnej obsługującej wykryte zdarzenia
 *
 */
void button_register_handler(button_t *const button,
		void (*handler)(button_event_t event)) __NONNULL_ARG(1);

/**
 * resetuje dekoder
 *
 * \param button	wskaźnik do przycisku
 *
 */
void button_reset(button_t *const button);

/**
 * uaktualnia stan dekodera
 * - wywołuje zarejestrowaną funkcję po wykryciu zdarzenia
 * - musi być wywoływana ze stałym interwałem czasowym
 *
 * \param button	wskaźnik do przycisku
 * \param pressed	sprzętowy stan przycisku
 *
 */
void button_detect_event(button_t *const button, bool pressed) __NONNULL;

