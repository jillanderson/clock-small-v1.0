#pragma once

/** \file ********************************************************************
 *
 * makra generujące błąd kompilacji
 *
 *****************************************************************************/

#include <stdint.h>

/**
 * przerywa kompilację jeśli warunek jest prawdziwy "true"
 *
 * \param condition warunek, który kompilator powinien widzieć jako "false"
 *
 */
#define BUILD_BUG_ON(condition)	((void)sizeof(char[1U - 2U * !!(condition)]))

/**
 * wymusza błąd kompilacji jeśli wyrażenie jest prawdziwe "true",
 * - jeśli wyrażenie e jest rozwijane do 0 (e == 0), makro zwraca 0
 * - jeśli wyrażenie e jest rozwijane do wartości różnej od 0, makro generuje
 *   błąd w trakcie kompilacji.
 * - tworzy również wynik (wartość 0 typu size_t) więc wyrażenie może być użyte
 *   np jako inicjalizator struktury itp.
 *
 * (uwaga: niedozwolone jest użycie w wyrażeniach "comma expressions")
 *
 */
#define BUILD_BUG_ON_ZERO(e)	(sizeof(struct { int:-!!(e); }))

/**
 * wymusza błąd kompilacji jeśli wyrażenie jest prawdziwe "true",
 * - jeśli wyrażenie e jest rozwijane do 0 (e == 0), makro zwraca (void*)0
 * - jeśli wyrażenie e jest rozwijane do wartości różnej od 0, makro generuje
 *   błąd w trakcie kompilacji.
 * - tworzy również wynik (wartość (void*)0 typu size_t) więc wyrażenie może
 *   być użyte np jako inicjalizator struktury itp.
 *
 * (uwaga: niedozwolone jest użycie w wyrażeniach "comma expressions")
 *
 */
#define BUILD_BUG_ON_NULL(e)	((void *)sizeof(struct { int:-!!(e); }))

/**
 * wymusza błąd kompilacji jeśli stałe wyrażenie n nie jest potęgą liczby 2 [2^n]
 *
 */
#define __BUILD_BUG_ON_NOT_POWER_OF_2(n) \
		BUILD_BUG_ON(((n) & ((n) - 1U)) != 0U)

/**
 * wymusza błąd kompilacji jeśli stałe wyrażenie n == 0
 * lub nie jest potęgą liczby 2 [2^n]
 *
 */
#define BUILD_BUG_ON_NOT_POWER_OF_2(n) \
		BUILD_BUG_ON((n) == 0U || (((n) & ((n) - 1U)) != 0U))

/**
 * sprawdza czy typy/zmienne a i b są tego samego typu
 *
 * (uwaga: korzysta w wbudowanej funkcji kompilatora gcc)
 *
 */
#define SAME_TYPE(a, b)	__builtin_types_compatible_p(typeof((a)), typeof((b)))

/**
 * wymusza błąd kompilacji jeśli a nie jest wskaźnikiem typu array
 *
 * (uwaga: &a[0] upraszcza się do zwykłego wskaźnika, który jest innego typu
 * niż tablica)
 *
 */
#define __must_be_array(a)	BUILD_BUG_ON_ZERO(SAME_TYPE((a), &(a)[0U]))

/**
 * wymusza błąd kompilacji jeśli parametr p nie jest wskaźnikiem lecz
 * liczbą lub wskaźnikem void*
 *
 * (uwaga: dereferencja może być wykonywana tylko na wskaźnikach)
 *
 */
#define __must_be_pointer(p) 	((p) + (sizeof(*(p)) * 0U))

/**
 * wymusza błąd kompilacji jeśli parametr n nie jest liczbą lecz wskaźnikiem
 *
 * (uwaga: liczby mogą być mnożone, wskaźniki nie)
 *
 */
#define __must_be_number(n)	((n) * 1U)

/**
 * wymusza błąd kompilacji jeśli liczba i nie jest całkowita
 *
 * (uwaga: tylko liczby całkowite można przesuwać bitowo)
 *
 */
#define __must_be_intiger(i)	((i) << 0U)

