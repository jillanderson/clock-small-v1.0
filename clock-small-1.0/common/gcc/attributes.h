#pragma once

/** \file ********************************************************************
 *
 * wybrane atrybuty kompilatora gcc
 *
 *****************************************************************************/

/** \name atrybuty_instrukcji
 */
/**\{*/
/** blokuje ostrzeżenie kompilatora np. przy braku \b break w sekcji \b case */
#define __FALLTHROUGH		__attribute__((__fallthrough__))
/**\}*/

/** \name atrybuty_funkcji
 */
/**\{*/
#define __ALWAYS_INLINE		inline __attribute__((__always_inline__))
#define __NO_INLINE		__attribute__((__noinline__))
#define __COLD			__attribute__((__cold__))
#define __HOT			__attribute__((__hot__))
#define __CONST			__attribute__((__const__))
#define __PURE			__attribute__((__pure__))
#define __FLATTEN		__attribute__((__flatten__))
#define __NONNULL		__attribute__((__nonnull__))
#define __NONNULL_ARG(...)	__attribute__((__nonnull__(__VA_ARGS__)))
#define __RETURNS_NONNULL	__attribute__((__returns_nonnull__))
#define __NO_RETURN		__attribute__((__noreturn__))
#define __WEAK			__attribute__((__weak__))
/**\}*/

/** \name atrybuty_funkcji_avr
 */
/**\{*/
#define __NAKED			__attribute__((__naked__))
#define __OS_MAIN		__attribute__((__OS_main__))
#define __OS_TASK		__attribute__((__OS_task__))
/**\}*/

/** \name atrybuty_wspólne_funkcji_i_zmiennych
 */
/**\{*/
#define __USED			__attribute__((__used__))
#define __UNUSED		__attribute__((__unused__))
#define __MAYBE_UNUSED		__attribute__((__unused__))
/**\}*/

/** \name atrybuty_zmiennych
 */
/**\{*/
#define __PACKED		__attribute__((__packed__))
#define __IO(addr)		__attribute__ ((io (addr)))
/**\}*/

