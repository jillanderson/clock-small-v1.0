#pragma once

/** \file ********************************************************************
 *
 * makra narzędziowe
 *
 * (uwaga: wykorzystują specyficzne właściwości kompilatora gcc)
 *
 *****************************************************************************/

#include "build_bug.h"

/**
 * zwraca wersję kompilatora gcc
 *
 */
#define GCC_VERSION \
		(__GNUC__ * 10000U + __GNUC_MINOR__ * 100U + __GNUC_PATCHLEVEL__)

/**
 * bariera dla optymalizacji
 *
 * zapobiega zmianie kolejności wykonania instrukcji
 *
 */
#define barrier() __asm__ __volatile__("": : :"memory")

/**
 * wyłącza zmienną z procesu optymalizacji
 *
 */
#define optimizer_hide_var(var)	__asm__ ("" : "=r" (var) : "0" (var))

/**
 * ukrywa ostrzeżenie kompilatora o niezainicjowanej zmiennej
 *
 * (uwaga: nie generuje żadnego kodu)
 *
 */
#define UNINITIALIZED_VAR(x)	x = x

/**
 * zwraca wyrażony w bajtach offset elementu (member)
 * umieszczonego wewnątrz stuktury lub unii (type)
 *
 * (uwaga: nie musi istnieć zdeklarowana zmienna typu (type))
 *
 */
#if defined(__builtin_offsetof)
#define offset_of(type, member)		__builtin_offsetof(type, member)
#else
#define offset_of(type, member)		((size_t) &(((type *)0)->member))
#endif

/**
 * zwraca wyrażony w bajtach rozmiar elementu (field), umieszczonego wewnątrz
 * stuktury lub unii (type)
 *
 * (uwaga: nie musi istnieć zdeklarowana zmienna typu (type))
 *
 */
#define field_size_of(type, field)	((size_t) sizeof(((type *)0)->field))

/**
 * rzutuje element struktury na bazową strukturę-kontener w której jest zawarta
 *
 * \param ptr	wskaźnik do elementu struktury-kontenera
 * \param type 	typ struktury-kontenera w którym zawiera się dany element
 * \param	member	nazwa elementu znajdującego się w strukturze-kontenerze
 * 		na którą wskazuje wskaźnik ptr
 *
 */
#define CONTAINER_OF(ptr, type, member) ({ \
		const typeof(((type *)0)->member) *_mptr = (ptr); \
		(type *)((char *)_mptr - offset_of(type,member)); })

/**
 * zapisuje vartość val do zmiennej x rzutowanej na typ volatile
 *
 * (uwaga: wymusza pominięcie optymalizacji kompilatora)
 *
 */
#define WRITE_ONCE(x, val)	((*((volatile typeof((val))*)(&(x)))) = (val))

/**
 * odzytuje wartość zmiennej x rzutowanej na typ volatile
 *
 * (uwaga: wymusza pominięcie optymalizacji kompilatora)
 *
 */
#define READ_ONCE(x)	(*((volatile typeof((x))*)(&(x))))

/**
 * zwraca liczbę elementów \b jawnie zdeklarowanej tablicy "a"
 *
 * (uwaga: nie działa ze wskaźnikami, tablicami zdeklarowanymi jako []
 *         lub tablicami będącymi parametrem funkcji)
 *
 */
#define array_size(a)	(sizeof((a)) / sizeof((a)[0]) + __must_be_array((a)))

/**
 * sprawdza czy struktury a i b tego samego typu są bitowo identyczne
 *
 * (uwaga: including padding)
 *
 */
#define STRUCTS_EQUAL(a, b) \
		(memcmp((a), (b), sizeof(*(a)) + 0 * sizeof((a) == (b))) == 0)

