#pragma once

/** \file ********************************************************************
 *
 * sterowanie wejściem dla sygnału przerwania zegara zewnętrznego
 *
 * note\ Interrupt output PCF8583
 * 	The conditions for activating the output INT (active LOW)
 * 	are determined by appropriate programming of the alarm control
 * 	register.
 * 	These conditions are clock alarm, timer alarm, timer overflow,
 * 	and event counter alarm.
 * 	An interrupt occurs when the alarm flag or the timer flag is set,
 * 	and the corresponding interrupt is enabled.
 * 	In all events, the interrupt is cleared only by software resetting
 * 	of the flag which initiated the interrupt.In the clock mode,
 * 	if the alarm enable is not activated (alarm enable bit of the control
 * 	and status register is logic 0), the interrupt output toggles
 * 	at 1 Hz with a 50 % duty cycle (may be used for calibration).
 * 	This is the default power-on state of the device.
 *
 *****************************************************************************/

/**
 * włącza obsługę przerwań od zegara
 *
 */
void drv_external_interrupt_start(void);

/**
 * wyłącza obsługę przerwań od zegara
 *
 */
void drv_external_interrupt_stop(void);

/**
 * rejestruje funkcję zwrotną
 *
 * \param handler funkcja zwrotna dla przerwania
 *
 */
void drv_external_interrupt_handler(void (*handler)(void));

