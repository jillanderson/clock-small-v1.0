#pragma once

/** \file ********************************************************************
 *
 * obsługa zegara PCF8583
 *
 *****************************************************************************/

#include <stdint.h>
#include <stdbool.h>

typedef struct pcf8583_date_time {
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t hsecond;
} pcf8583_time_t;

/**
 * inicjalizacja zegara w trybie 24h
 *
 */
void drv_pcf8583_init(void);

/**
 * uruchamia pracę zegara
 *
 */
void drv_pcf8583_start(void);

/**
 * inicjalizacja zegara w trybie 24h
 *
 * \return true jeśli zegar jest zresetowany
 * 	   false w przeciwnym przypadku
 *
 * \note pozwala określić czy zegar został uruchomiony po resecie
 * 	 np. w wyniku zaniku zasilania
 *
 */
bool drv_pcf8583_is_reset(void);

/**
 * odczytuje z zegara datę i czas
 *
 * \param time struktura przechowująca datę i czas
 *
 */
void drv_pcf8583_read_time(pcf8583_time_t *const time);

/**
 * zapisuje do zegara podaną datę i czas
 *
 * \param time struktura przechowująca datę i czas
 *
 */
void drv_pcf8583_write_time(pcf8583_time_t const *const time);

