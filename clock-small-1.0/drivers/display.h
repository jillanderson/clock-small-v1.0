#pragma once

/** \file ********************************************************************
 *
 * multiplexowana obsługa wyświetlacza siedmiosegmentowego
 *
 *****************************************************************************/

#include <stdint.h>

typedef enum brightness {
	BRIGHTNESS_STEP_MINIMUM = 0U,
	BRIGHTNESS_STEP_25_PERCENT,
	BRIGHTNESS_STEP_50_PERCENT,
	BRIGHTNESS_STEP_75_PERCENT,
	BRIGHTNESS_STEP_MAXIMUM
} brightness_t;

typedef enum segment {
	HOURS_TENS = 0U,
	HOURS_UNITS = 1U,
	COLON_HM = 2U,
	MINUTES_TENS = 3U,
	MINUTES_UNITS = 4U,
	COLON_MS = 5U,
	SECONDS_TENS = 6U,
	SECONDS_UNITS = 7U
} segment_t;

typedef enum symbol_extra {
	DIGIT_NONE = 10,
	MINUS = 11,
	COLON_ON = 12,
	COLON_OFF = 13
} symbol_extra_t;

/**
 * inicjalizuje obsługę wyświetlacza
 *
 * \param refresh_Hz 	częstotliwość odświerzania segmentu wyświetlacza
 * \param brightness	jasność wyświetlacza tylko zdefiniowane wartości
 *
 */
void drv_display_init(uint8_t refresh_Hz, brightness_t step);

/**
 * ustawia jasność wyświetlacza
 *
 * \param step	numer kroku jesności wyświetlacza
 *
 */
void drv_display_brightness(brightness_t step);

/**
 * wyświetla podany symbol na podanym segmencie wyświetlacza
 *
 * \param segment 	segment wyświetlacza
 * \param symbol	wyświetlany znak [1 ... 9]
 * 			oraz zdefiniowane znaki specjalne
 *
 */
void drv_display_show(segment_t segment, uint8_t symbol);

