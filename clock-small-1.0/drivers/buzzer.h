#pragma once

/** \file ********************************************************************
 *
 * sterowanie brzęczykiem
 *
 *****************************************************************************/

#include <stdint.h>

/**
 * inicjalizuje obsługę brzęczyka
 *
 * \param tick	wewnętrzna podstawa czasu będąca krotnością systemowej
 * 		podstawy czasu
 *
 */
void drv_buzzer_init(uint8_t tick);

/**
 * włącza brzęczyk
 *
 */
void drv_buzzer_turn_on(void);

/**
 * wyłącza brzęczyk
 *
 */
void drv_buzzer_turn_off(void);

/**
 * włącza brzęczyk z zadaną ilością dźwięków o zadanej długości
 *
 * \param number	zadana ilość dźwięków brzęczyka:
 * 				- number > 0	podana liczba dźwięków
 * 				- number = 0	nieograniczona liczba dźwięków
 *
 * \param period_tick	czas włączeia/wyłączenia brzęczyka będący krotnością
 * 			wewnętrznej podstawy czasu modułu
 *
 */
void drv_buzzer_turn_beep(uint8_t number, uint8_t period_tick);

