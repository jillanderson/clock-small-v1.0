#include "../../hal/indicator.h"
#include <stdint.h>

#include "../../system/scheduler.h"
#include "../indicator_orange.h"

static struct indicator_orange {
	struct {
		uint8_t number;
		uint8_t period;
		uint16_t number_counter;
		uint8_t period_counter;
	} blink;

	task_t task;
} indicator_orange = {0};

static void indicator_orange_task_handler(void *arg);

void drv_indicator_orange_init(uint8_t tick)
{
	hal_indicator_turn_off(INDICATOR_ORANGE);
	(indicator_orange.blink).number = 0U;
	(indicator_orange.blink).period = 0U;
	(indicator_orange.blink).number_counter = 0U;
	(indicator_orange.blink).period_counter = 0U;
	scheduler_add_task(&(indicator_orange.task), indicator_orange_task_handler, &indicator_orange, 0U, tick);
}

void drv_indicator_orange_turn_on(void)
{
	hal_indicator_turn_on(INDICATOR_ORANGE);
	scheduler_stop_task(&(indicator_orange.task));
}

void drv_indicator_orange_turn_off(void)
{
	hal_indicator_turn_off(INDICATOR_ORANGE);
	scheduler_stop_task(&(indicator_orange.task));
}

void drv_indicator_orange_turn_blink(uint8_t number, uint8_t period_tick)
{
	if (0U < period_tick) {

		hal_indicator_turn_off(INDICATOR_ORANGE);
		(indicator_orange.blink).number = number;
		(indicator_orange.blink).period = period_tick;
		(indicator_orange.blink).number_counter = number * ((uint16_t)2U);
		(indicator_orange.blink).period_counter = period_tick;
		scheduler_start_task(&(indicator_orange.task));
	}
}

void indicator_orange_task_handler(void *arg)
{
	struct indicator_orange *indicator_orange_arg = (struct indicator_orange*)arg;

	if (0U == --((indicator_orange_arg->blink).period_counter)) {

		hal_indicator_toggle(INDICATOR_ORANGE);
		(indicator_orange_arg->blink).period_counter = (indicator_orange_arg->blink).period;

		if (0U < (indicator_orange_arg->blink).number) {

			if (0U == --((indicator_orange_arg->blink).number_counter)) {
				scheduler_stop_task(&(indicator_orange_arg->task));
			}
		}
	}
}

