#include <avr/interrupt.h>

#include "../../hal/int1.h"
#include "../extint1.h"

void drv_external_interrupt_start(void)
{
	hal_external_int1_start();
}

void drv_external_interrupt_stop(void)
{
	hal_external_int1_stop();
}

void drv_external_interrupt_handler(void (*handler)(void))
{
	hal_external_int1_add_handler(handler);
}

