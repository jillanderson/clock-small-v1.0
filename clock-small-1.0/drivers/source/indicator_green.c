#include "../../hal/indicator.h"
#include "../indicator_green.h"

#include <stdint.h>

#include "../../system/scheduler.h"

static struct indicator_green {
	struct {
		uint8_t number;
		uint8_t period;
		uint16_t number_counter;
		uint8_t period_counter;
	} blink;

	task_t task;
} indicator_green = {0};

static void indicator_green_task_handler(void *arg);

void drv_indicator_green_init(uint8_t tick)
{
	hal_indicator_turn_off(INDICATOR_GREEN);
	(indicator_green.blink).number = 0U;
	(indicator_green.blink).period = 0U;
	(indicator_green.blink).number_counter = 0U;
	(indicator_green.blink).period_counter = 0U;
	scheduler_add_task(&(indicator_green.task), indicator_green_task_handler, &indicator_green, 0U, tick);
}

void drv_indicator_green_turn_on(void)
{
	hal_indicator_turn_on(INDICATOR_GREEN);
	scheduler_stop_task(&(indicator_green.task));
}

void drv_indicator_green_turn_off(void)
{
	hal_indicator_turn_off(INDICATOR_GREEN);
	scheduler_stop_task(&(indicator_green.task));
}

void drv_indicator_green_turn_blink(uint8_t number, uint8_t period_tick)
{
	if (0U < period_tick) {

		hal_indicator_turn_off(INDICATOR_GREEN);
		(indicator_green.blink).number = number;
		(indicator_green.blink).period = period_tick;
		(indicator_green.blink).number_counter = number * ((uint16_t)2U);
		(indicator_green.blink).period_counter = period_tick;
		scheduler_start_task(&(indicator_green.task));
	}
}

void indicator_green_task_handler(void *arg)
{
	struct indicator_green *indicator_green_arg = (struct indicator_green*)arg;

	if (0U == --((indicator_green_arg->blink).period_counter)) {

		hal_indicator_toggle(INDICATOR_GREEN);
		(indicator_green_arg->blink).period_counter = (indicator_green_arg->blink).period;

		if (0U < (indicator_green_arg->blink).number) {

			if (0U == --((indicator_green_arg->blink).number_counter)) {
				scheduler_stop_task(&(indicator_green_arg->task));
			}
		}
	}
}

