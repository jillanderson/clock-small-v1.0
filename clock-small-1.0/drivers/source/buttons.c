#include "../buttons.h"

#include <stdbool.h>
#include <stdint.h>

#include "../../common/button/button.h"
#include "../../common/gcc/utils.h"
#include "../../hal/button.h"
#include "../../system/scheduler.h"

static uint8_t const id_assignment[] = {BUTTON_A, BUTTON_B, BUTTON_C, BUTTON_D, BUTTON_E};
static uint8_t const id_assignment_qty = array_size(id_assignment);

static button_t button_detector[] = {{0}, {0}, {0}, {0}, {0}};
static uint8_t const button_detector_qty = array_size(button_detector);

static struct button_control {
	button_t *decoder;
	uint8_t const *id;
	task_t task;
} button_control;

static void button_control_task_handler(void *arg);

void drv_button_init(uint8_t tick, uint8_t debounce_tick,
		uint8_t double_click_tick, uint8_t long_press_start_tick,
		uint8_t long_press_hold_repeat_tick)
{
	button_control.decoder = button_detector;
	button_control.id = id_assignment;

	for (uint8_t i = 0U; button_detector_qty > i; i++) {

		button_init(&(button_control.decoder[i]), debounce_tick,
				double_click_tick, long_press_start_tick,
				long_press_hold_repeat_tick);
	}

	scheduler_add_task(&(button_control.task), button_control_task_handler,
			&button_control, 0U, tick);
}

void drv_button_handler(button_function_id_t id,
		void (*handler)(button_event_t event))
{
	if ((id_assignment_qty > id) && (id_assignment_qty <= button_detector_qty)) {
		button_register_handler(&(button_control.decoder[id]), handler);
	}
}

void drv_button_start(void)
{
	scheduler_start_task(&(button_control.task));
}

void drv_button_stop(void)
{
	scheduler_stop_task(&(button_control.task));

	for (uint8_t i = 0U; button_detector_qty > i; i++) {
		button_reset(&(button_control.decoder[i]));
	}
}

void button_control_task_handler(void *arg)
{
	struct button_control *button_control_arg = (struct button_control*)arg;

	for (uint8_t i = 0U; id_assignment_qty > i; i++) {
		button_detect_event(&(button_control_arg->decoder[i]),
				hal_button_is_pressed(button_control_arg->id[i]));
	}
}

