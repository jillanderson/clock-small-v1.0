#include "../pcf8583.h"

#include <stdbool.h>
#include <stdint.h>

#include "../../common/general/bcd.h"
#include "../../hal/twi.h"

static uint8_t const pcf8583_address = 0xA0U;
static uint8_t const pcf8583_twi_speed_kHz = 100U;

static uint8_t pcf8583_read_byte(uint8_t address);
static void pcf8583_write_byte(uint8_t address, uint8_t data);
static void pcf8583_write_word(uint8_t address, uint16_t data);
static uint8_t pcf8583_read_status(void);
static void pcf8583_start(void);
static void pcf8583_stop(void);
static void pcf8583_count_store_and_hold(void);
static void pcf8583_count_restore_and_continue(void);

void drv_pcf8583_init(void)
{
	hal_twi_init(pcf8583_twi_speed_kHz);
	/* reset ustawień, taktowanie 32768Hz */
	pcf8583_write_byte(0U, 0U);
	/* format 24h */
	pcf8583_write_byte(4U, pcf8583_read_byte(4U) & 0x3fU);

	pcf8583_stop();
}

void drv_pcf8583_start(void)
{
	pcf8583_start();
}

bool drv_pcf8583_is_reset(void)
{
	uint8_t hours_register = pcf8583_read_byte(4U);
	uint8_t year_date_register = pcf8583_read_byte(5U);
	uint8_t months_register = pcf8583_read_byte(6U);

	bool result = false;

	if ((hours_register == 0U) && (year_date_register == 1U)
			&& (months_register == 1U)) {
		result = true;
	}

	return (result);
}

void drv_pcf8583_read_time(pcf8583_time_t *const time)
{
	pcf8583_count_store_and_hold();
	/* odczyt czasu */
	time->hsecond = bcd2bin(pcf8583_read_byte(1U));
	time->second = bcd2bin(pcf8583_read_byte(2U));
	time->minute = bcd2bin(pcf8583_read_byte(3U));
	time->hour = bcd2bin(pcf8583_read_byte(4U));
	/* odczyt daty */
	uint8_t dy = pcf8583_read_byte(5U);
	time->month = bcd2bin(pcf8583_read_byte(6U) & 0x1FU);
	time->day = bcd2bin(dy & 0x3FU);
	dy >>= 6U;
	uint16_t y1 = pcf8583_read_byte(16U)
			| ((uint16_t)pcf8583_read_byte(17U) << 8U);

	if (((uint8_t)y1 & 3U) != dy) {
		pcf8583_write_word(16U, ++y1);
	}

	time->year = y1;

	pcf8583_count_restore_and_continue();
}

void drv_pcf8583_write_time(pcf8583_time_t const *const time)
{
	pcf8583_stop();
	/* wpisujemy czas */
	pcf8583_write_byte(1U, bin2bcd(time->hsecond));
	pcf8583_write_byte(2U, bin2bcd(time->second));
	pcf8583_write_byte(3U, bin2bcd(time->minute));
	pcf8583_write_byte(4U, bin2bcd(time->hour));
	/* wpisujemy datę */
	pcf8583_write_word(16U, time->year);
	pcf8583_write_byte(5U,
			bin2bcd(time->day)
					| (((uint8_t)time->year & 3U) << 6U));
	pcf8583_write_byte(6U, bin2bcd(time->month));

	pcf8583_start();
}

uint8_t pcf8583_read_byte(uint8_t address)
{
	while (hal_twi_send_start_and_select(pcf8583_address + TW_WRITE)) {
		;
	}

	hal_twi_send_byte(address);

	while (hal_twi_send_repeat_start_and_select(pcf8583_address + TW_READ)) {
		;
	}

	uint8_t data = hal_twi_receive_data_nack();

	hal_twi_stop();

	return (data);
}

void pcf8583_write_byte(uint8_t address, uint8_t data)
{
	while (hal_twi_send_start_and_select(pcf8583_address + TW_WRITE)) {
		;
	}

	hal_twi_send_byte(address);
	hal_twi_send_byte(data);
	hal_twi_stop();
}

void pcf8583_write_word(uint8_t address, uint16_t data)
{
	pcf8583_write_byte(address, (uint8_t)(data & 0x00ffU));
	address += 1;
	pcf8583_write_byte(address, (uint8_t)(data >> 8U));
}

uint8_t pcf8583_read_status(void)
{
	uint8_t status = pcf8583_read_byte(0U);

	return (status);
}

void pcf8583_start(void)
{
	uint8_t status = pcf8583_read_status();

	status &= 0x7fU;
	pcf8583_write_byte(0U, status);
}

void pcf8583_stop(void)
{
	uint8_t status = pcf8583_read_status();

	status |= 0x80U;
	pcf8583_write_byte(0U, status);
}

void pcf8583_count_store_and_hold(void)
{
	uint8_t status = pcf8583_read_status();

	status |= 0x40U;
	pcf8583_write_byte(0U, status);
}

void pcf8583_count_restore_and_continue(void)
{
	uint8_t status = pcf8583_read_status();

	status &= 0xbfU;
	pcf8583_write_byte(0U, status);
}

