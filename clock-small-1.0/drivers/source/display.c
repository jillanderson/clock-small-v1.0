#include "../../hal/display.h"
#include "../display.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>

#include "../../common/gcc/utils.h"
#include "../../hal/timer1.h"

/** \note wyświetlacz 7 seg
 *
 *          A
 *         ---
 *      F | G | B
 *         ---
 *      E |   | C
 *         ---  . DP
 *          D
 *
 */

static uint8_t const anode_sequence[] = {ANODE_0, ANODE_1, ANODE_COLON_0,
		ANODE_2, ANODE_3, ANODE_COLON_1, ANODE_4, ANODE_5};
static uint8_t const anode_sequence_qty = array_size(anode_sequence);

static uint8_t const correction[] = {15, 25, 45, 70, 90};
static uint8_t const correction_qty = array_size(correction);

static struct display {
	uint16_t brightness_compare_maximum;
	uint16_t brightness_compare;
	uint8_t *symbol;
} display = {0};

static void display_off();

static void digit_0(void);
static void digit_1(void);
static void digit_2(void);
static void digit_3(void);
static void digit_4(void);
static void digit_5(void);
static void digit_6(void);
static void digit_7(void);
static void digit_8(void);
static void digit_9(void);
static void digit_none(void);
static void colon(void);
static void colon_none(void);

static void show_symbol(uint8_t symbol);

static uint16_t compare_value(uint8_t compare_percent);

static void timer1_compare_a_handler(void);
static void timer1_compare_b_handler(void);

void drv_display_init(uint8_t refresh_Hz, brightness_t step)
{
	uint16_t const factor[] = {0U, 1U, 8U, 64U, 256U, 1024U};
	uint32_t const ticks = (uint32_t)(F_CPU
			/ (uint16_t)(refresh_Hz * anode_sequence_qty));
	uint16_t const factor_top =
			(0U == (ticks % UINT16_MAX)) ?
					((uint16_t)(ticks / UINT16_MAX)) :
					((uint16_t)((ticks / UINT16_MAX) + 1U));

	uint8_t presc_number = 0U;
	uint16_t top_value = 0U;

	while (factor_top <= factor[array_size(factor) - 1U]) {
		if (factor_top > factor[presc_number]) {
			presc_number++;
		} else {
			uint16_t ticks_factor =
					(uint16_t)(((((uint32_t)ticks * 2U)
							/ factor[presc_number])
							+ 1U) / 2U);
			top_value = (ticks_factor - 1U);
			break;
		}
	}

	display.brightness_compare_maximum = top_value;
	display.brightness_compare = compare_value(correction[step]);

	display.symbol = calloc(anode_sequence_qty, sizeof(*(display.symbol)));

	display.symbol[HOURS_TENS] = MINUS;
	display.symbol[HOURS_UNITS] = MINUS;
	display.symbol[COLON_HM] = COLON_OFF;
	display.symbol[MINUTES_TENS] = MINUS;
	display.symbol[MINUTES_UNITS] = MINUS;
	display.symbol[COLON_MS] = COLON_OFF;
	display.symbol[SECONDS_TENS] = MINUS;
	display.symbol[SECONDS_UNITS] = MINUS;

	hal_timer1_start(presc_number, top_value, display.brightness_compare);
	hal_timer1_add_compare_handler(timer1_compare_a_handler,
			timer1_compare_b_handler);

}

void drv_display_brightness(brightness_t step)
{
	if ((correction_qty > step)) {
		display.brightness_compare = compare_value(
				correction[step]);
	}
}

void drv_display_show(segment_t segment, uint8_t symbol)
{
	display.symbol[segment] = symbol;
}

void display_off()
{
	for (uint8_t i = 0; anode_sequence_qty > i; i++) {
		hal_display_anode_turn_off(i);
	}
}

void digit_0(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_on(SEGMENT_D);
	hal_display_segment_turn_on(SEGMENT_E);
	hal_display_segment_turn_on(SEGMENT_F);
	hal_display_segment_turn_off(SEGMENT_G);
}

void digit_1(void)
{
	hal_display_segment_turn_off(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_off(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_off(SEGMENT_F);
	hal_display_segment_turn_off(SEGMENT_G);
}

void digit_2(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_off(SEGMENT_C);
	hal_display_segment_turn_on(SEGMENT_D);
	hal_display_segment_turn_on(SEGMENT_E);
	hal_display_segment_turn_off(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void digit_3(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_on(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_off(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void digit_4(void)
{
	hal_display_segment_turn_off(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_off(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_on(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void digit_5(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_off(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_on(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_on(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void digit_6(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_off(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_on(SEGMENT_D);
	hal_display_segment_turn_on(SEGMENT_E);
	hal_display_segment_turn_on(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void digit_7(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_off(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_off(SEGMENT_F);
	hal_display_segment_turn_off(SEGMENT_G);
}

void digit_8(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_on(SEGMENT_D);
	hal_display_segment_turn_on(SEGMENT_E);
	hal_display_segment_turn_on(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void digit_9(void)
{
	hal_display_segment_turn_on(SEGMENT_A);
	hal_display_segment_turn_on(SEGMENT_B);
	hal_display_segment_turn_on(SEGMENT_C);
	hal_display_segment_turn_on(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_on(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void digit_none(void)
{
	hal_display_segment_turn_off(SEGMENT_A);
	hal_display_segment_turn_off(SEGMENT_B);
	hal_display_segment_turn_off(SEGMENT_C);
	hal_display_segment_turn_off(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_off(SEGMENT_F);
	hal_display_segment_turn_off(SEGMENT_G);
}

void minus(void)
{
	hal_display_segment_turn_off(SEGMENT_A);
	hal_display_segment_turn_off(SEGMENT_B);
	hal_display_segment_turn_off(SEGMENT_C);
	hal_display_segment_turn_off(SEGMENT_D);
	hal_display_segment_turn_off(SEGMENT_E);
	hal_display_segment_turn_off(SEGMENT_F);
	hal_display_segment_turn_on(SEGMENT_G);
}

void colon(void)
{
	hal_display_segment_turn_on(SEGMENT_COLON);
}

void colon_none(void)
{
	hal_display_segment_turn_off(SEGMENT_COLON);
}

void show_symbol(uint8_t symbol)
{
	switch (symbol) {
	case 0:
		digit_0();
		break;
	case 1:
		digit_1();
		break;
	case 2:
		digit_2();
		break;
	case 3:
		digit_3();
		break;
	case 4:
		digit_4();
		break;
	case 5:
		digit_5();
		break;
	case 6:
		digit_6();
		break;
	case 7:
		digit_7();
		break;
	case 8:
		digit_8();
		break;
	case 9:
		digit_9();
		break;
	case 10:
		digit_none();
		break;
	case 11:
		minus();
		break;
	case 12:
		colon();
		break;
	case 13:
		colon_none();
		break;
	default:
		digit_none();
		colon_none();
		break;
	}
}

uint16_t compare_value(uint8_t compare_percent)
{
	uint16_t value = (uint16_t)(((uint32_t)(display.brightness_compare_maximum)
			* compare_percent) / 100U);

	return (value);
}

void timer1_compare_a_handler(void)
{
	static uint8_t anode_active = 0;

	show_symbol(display.symbol[anode_active]);
	hal_display_anode_turn_on(anode_sequence[anode_active]);

	anode_active = (anode_active + 1) % anode_sequence_qty;

	OCR1B = display.brightness_compare;
}

void timer1_compare_b_handler(void)
{
	display_off();
}

