#include "../../hal/buzzer.h"
#include "../buzzer.h"

#include <stdint.h>

#include "../../system/scheduler.h"

static struct buzzer {
	struct {
		uint8_t number;
		uint8_t period;
		uint16_t number_counter;
		uint8_t period_counter;
	} beep;

	task_t task;
} buzzer = {0};

static void buzzer_task_handler(void *arg);

void drv_buzzer_init(uint8_t tick)
{
	hal_buzzer_generator_turn_off();
	(buzzer.beep).number = 0U;
	(buzzer.beep).period = 0U;
	(buzzer.beep).number_counter = 0U;
	(buzzer.beep).period_counter = 0U;
	scheduler_add_task(&(buzzer.task), buzzer_task_handler,
			&buzzer, 0U, tick);
}

void drv_buzzer_turn_on(void)
{
	hal_buzzer_generator_turn_on();
	scheduler_stop_task(&(buzzer.task));
}

void drv_buzzer_turn_off(void)
{
	hal_buzzer_generator_turn_off();
	scheduler_stop_task(&(buzzer.task));
}

void drv_buzzer_turn_beep(uint8_t number, uint8_t period_tick)
{
	if (0U < period_tick) {

		hal_buzzer_generator_turn_off();
		(buzzer.beep).number = number;
		(buzzer.beep).period = period_tick;
		(buzzer.beep).number_counter = number * ((uint16_t)2U);
		(buzzer.beep).period_counter = period_tick;
		scheduler_start_task(&(buzzer.task));
	}
}

void buzzer_task_handler(void *arg)
{
	struct buzzer *buzzer_arg = (struct buzzer*)arg;

	if (0U == --((buzzer_arg->beep).period_counter)) {

		hal_buzzer_generator_toggle();
		(buzzer_arg->beep).period_counter =
				(buzzer_arg->beep).period;

		if (0U < (buzzer_arg->beep).number) {

			if (0U == --((buzzer_arg->beep).number_counter)) {
				scheduler_stop_task(&(buzzer_arg->task));
			}
		}
	}
}

