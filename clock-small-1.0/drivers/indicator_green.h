#pragma once

/** \file ********************************************************************
 *
 * sterowanie brzęczykiem
 *
 *****************************************************************************/

#include <stdint.h>

/**
 * inicjalizuje obsługę wskaźnika
 *
 * \param tick	wewnętrzna podstawa czasu będąca krotnością systemowej
 * 		podstawy czasu
 *
 */
void drv_indicator_green_init(uint8_t tick);

/**
 * włącza wskaźnik
 *
 */
void drv_indicator_green_turn_on(void);

/**
 * wyłącza wskaźnik
 *
 */
void drv_indicator_green_turn_off(void);

/**
 * włącza wskaźnik z zadaną ilością błysków o zadanej długości
 *
 * \param number	zadana ilość błysków wskaźnika:
 * 				- number > 0	podana liczba błysków
 * 				- number = 0	nieograniczona liczba błysków
 *
 * \param period_tick	czas włączenia/wyłączenia wskaźnika będący krotnością
 * 			wewnętrznej podstawy czasu modułu
 *
 */
void drv_indicator_green_turn_blink(uint8_t number, uint8_t period_tick);

