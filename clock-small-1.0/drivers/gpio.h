#pragma once

/** \file ********************************************************************
 *
 * konfiguracja pinów IO procesora
 *
 *****************************************************************************/

/**
 * początkowa konfiguracja pinów IO procesora
 *
 */
void drv_gpio_init(void);

