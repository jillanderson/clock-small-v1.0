/** \file ********************************************************************
 *
 * Sterownik zegara Clock-Small v1.0
 *
 * Copyright (C) 2020r.
 * GNU Public License
 *
 * funkcje:
 * 	- wyświetlanie czasu
 * 	- wyświetlanie daty
 * 	- ustawianie daty i czasu
 *
 * sterowanie:
 * 	- BUTTON_PREVIOUS
 * 	- BUTTON_MINUS
 * 	- BUTTON_SET
 * 	- BUTTON_PLUS
 * 	- BUTTON_NEXT
 *
 * zasoby sprzętowe:
 * 	- CPU			ATmega32
 * 	- F_CPU			8MHz oscylator wewnętrzny
 * 	- BOD			max 4,5V
 * 	- TIMER2_COMP_vect	systemowa podstawa czasu
 * 	- TIMER1_COMPB_vect	regulacja jasności wyświetlacza
 * 	- TIMER1_COMPA_vect	multiplexowanie wyświetlacza
 *
 * opcje kompilacji avr-gcc:
 * 	- opcje kompilatora:
 * 		"-Wall -Os -fpack-struct -fshort-enums -ffunction-sections
 * 		-fdata-sections -mcall-prologues -std=gnu99 -funsigned-char
 * 		-funsigned-bitfields -flto"
 *
 * 	- dodatkowe opcje kompilatora:
 * 		-Wextra -Wjump-misses-init -Wshadow -Wunused -Wsign-conversion
 * 		-Wlogical-op -Wjump-misses-init -Wshadow -Wunused
 * 		-Wsign-conversion -Wlogical-op

 * 	- opcje linkera:
 * 		"-Wl, --gc-sections, --relax -flto"
 *
 *****************************************************************************/

#include "common/gcc/attributes.h"
#include "common/general/definitions.h"

#include "device/clock.h"

/**
 * wykonuje aplikację sterującą zegarem
 *
 */
__OS_MAIN int main(void)
{
	dev_clock_init();

	for (;;) {
		dev_clock_run();
	}
	
	return (SUCCESS);
}

